# -*- coding: utf-8 -*-

import dash_core_components as dcc
import dash_html_components as html
import dash
from vision.app import app
import time
from dash.dependencies import Input, Output
from Data_Analysis.query import Query

# app = dash.Dash(__name__)

# layout = html.Div(
#     [
#         html.Div(
#             [
#             html.H3('keyword {}'.format(_)),
#             dcc.Input(
#                 id="input_{}".format(_),
#                 type='text',
#             )
#                 ]
#         )
#         for _ in range(1, 11)
#     ]
#     + [
#         html.Div(id="out-all-types"),
#        html.Button('Submit', id='submit-val', n_clicks=0)
#     ]
# )


layout = html.Div([
    html.H1('AI Engine'),
    html.Div([
        html.P('Query Name'),
        dcc.Input(
            id="query_name_input",
            type='text',
        )
    ], className='Width50Percent FloatLeft'),
    html.Div([
        html.P('Complexity'),
        # dcc.Dropdown(
        #     id='complexity-dropdown',
        #     options=[
        #         {'label': 'Low', 'value': 'low'},
        #         {'label': 'Medium', 'value': 'medium'},
        #         {'label': 'High', 'value': 'high'}
        #     ],
        #     value='low'
        # ),
        dcc.Slider(
            id="complexity_slider",
            min=1,
            max=10,
            step=None,
            marks={
                1: '1',
                2: '2',
                3: '3',
                4: '4',
                5: '5',
                6: '6',
                7: '7',
                8: '8',
                9: '9',
                10: '10',
            },
            value=1
        ),
    ], className='Width50Percent FloatLeft'),
    html.Div([
        html.P('Query'),
        dcc.Textarea(
            id='query-text',
            value='keywords or phrases to construct analytics...',
            style={'width': '100%', 'height': 100},
        ),
    ], className='FullWidth FloatLeft'),
    html.Div([
        html.Button('Create', id='query-button', className='ButtonStyle', n_clicks=0)
    ], className='Width50Percent FloatLeft'),
    dcc.Loading(
        id="building-analytics",
        type="default",
        children=html.Div(id="loading-output-1", className='FullWidth FloatLeft')
    ),
], className='LandingBodyDivStyle')


@app.callback(Output("loading-output-1", "children"),
              [Input("query-button", "n_clicks"), Input("query_name_input", "value"),
               Input("complexity_slider", "value"), Input("query-text", "value")])
def input_triggers_spinner(n_clicks, name, complexity, query_words):
    if n_clicks > 0:
        query_generator = Query()
        keywords = query_words.strip().split('\n')
        n_docs = complexity * 1000
        query_generator.query(name, keywords, n_docs, n_docs, n_docs)
        return f'{name} built and saved.'


# @app.callback(
#     Output("out-all-types", "children"),
#     [Input("input_{}".format(_), "value") for _ in range(1, 11)],
# )
# def cb_render(*vals):
#     return " | ".join((str(val) for val in vals if val))
#
#
# if __name__ == "__main__":
#     app.run_server(debug=True)
