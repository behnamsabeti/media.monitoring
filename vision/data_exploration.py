import json
import operator
import glob


class DataExploration:
    def __init__(self):
        self.sources = ['news', 'telegram', 'twitter']
        self.news_topics = []
        self.telegram_topics = []
        self.base_location = '/home/behnam/Military_Project/media.monitoring/analytics_results/'
        self.news_trend_file = '/news_trend.json'
        self.telegram_trend_file = '/telegram_trend.json'
        self.twitter_trend_file = '/twitter_trend.json'
        self.influencers_file = '/influencers.json'
        # self.news, self.posts, self.tweets, self.influencers = self.load_files()

    def query_names(self):
        query_names = glob.glob(self.base_location + '*/')
        query_names = [name.split('/')[-2] for name in query_names]
        return query_names

    def load_files(self, query):
        files = [self.base_location + query + self.news_trend_file, self.base_location + query + self.telegram_trend_file,
                 self.base_location + query + self.twitter_trend_file, self.base_location + query + self.influencers_file]
        news = []
        posts = []
        tweets = []
        influencers = []

        all_data = [news, posts, tweets, influencers]

        for index, file_loc in enumerate(files):
            with open(file_loc, 'r', encoding='utf8') as file:
                for line in file:
                    data = json.loads(line)
                    # if index == 2:
                    #     date = data['date']
                    #     start_date = '2019-06-01'
                    #     if date >= start_date:
                    #         all_data[index].append(data)
                    # else:
                    all_data[index].append(data)
                file.close()
            print('{} data loaded!'.format(file_loc))

        return all_data

    def __search(self, source, target):
        for item in source:
            if item in target:
                return True
        return False

    def __filter(self, source, topics, start, end):
        aggregated_topics = {}
        if source == 'news':
            items = self.news
            # for item in self.news_topics:
            #     aggregated_topics[item] = 0
        elif source == 'telegram':
            items = self.posts
            # for item in self.telegram_topics:
            #     aggregated_topics[item] = 0
        # else:
        #     items = self.tweets
        #     for item in self.twitter_topics:
        #         aggregated_topics[item] = 0
        result = []
        for item in items:
            # cat = item['category']
            topic = item['topics']
            # topic = list(topic.keys())
            date = item['date']
            # if self.__search(category, cat):
            if (start == '' or start <= date) and (end == '' or end >= date):
                for t in topic:
                    if t in aggregated_topics:
                        aggregated_topics[t] += topic[t]
                    else:
                        aggregated_topics[t] = topic[t]
                if self.__search(topics, topic):
                    result.append(item)
        if source == 'news':
            self.news_topics = list(aggregated_topics.keys())
        elif source == 'telegram':
            self.telegram_topics = list(aggregated_topics.keys())
        total = 0
        for t in aggregated_topics:
            total += aggregated_topics[t]
        for t in aggregated_topics:
            if total != 0:
                aggregated_topics[t] /= total
        return result, aggregated_topics

    def __check_parameters(self, source, topics):
        if source not in self.sources or not isinstance(source, str):
            print('Invalid source!')
            return False
        # for c in category:
        #     if c not in self.categories:
        #         print('Invalid category!')
        #         return False
        if source == 'news':
            current_topics = self.news_topics
        elif source == 'telegram':
            current_topics = self.telegram_topics
        # else:
        #     current_topics = self.twitter_topics
        for t in topics:
            if t not in current_topics:
                print('Invalid topics!')
                return False
        return True

    def __unify_on_dates(self, items):
        trend = dict()
        for item in items:
            date = item['date']
            if date not in trend:
                trend[date] = 1
            else:
                trend[date] += 1
        trend = sorted(trend.items(), key=operator.itemgetter(0))
        return trend

    def __update_dic(self, source, add):
        for item in add:
            if item in source:
                source[item] += add[item]
            else:
                source[item] = add[item]
        return source

    def __unify_on_dates_for_twitter(self, items):
        trend = dict()
        for item in items:
            date = item['date']
            emotions = item['emotion']
            sentiment = item['sentiment']
            profanity = item['profanity']
            retweet = item['retweet']
            p = 0
            r = 0
            if profanity:
                p = 1
            if retweet:
                r = 1
            if date not in trend:
                trend[date] = (1, emotions, sentiment, p, r)
            else:
                prev_emotions = trend[date][1]
                prev_sentiment = trend[date][2]
                prev_count = trend[date][0]
                prev_profanity = trend[date][3]
                prev_retweet = trend[date][4]
                new_sentiment = self.__update_dic(prev_sentiment, sentiment)
                new_emotion = self.__update_dic(prev_emotions, emotions)
                trend[date] = (prev_count+1, new_emotion, new_sentiment, prev_profanity+p, prev_retweet+r)
        new_trend = dict()
        trend_sentiment = dict()
        trend_emotion = dict()
        total_sentiment = dict()
        total_emotion = dict()
        for date in trend:
            count = trend[date][0]
            emotions = trend[date][1]
            sentiment = trend[date][2]
            profanity = trend[date][3]
            retweet = trend[date][4]
            for e in emotions:
                emotions[e] /= count
            for s in sentiment:
                sentiment[s] /= count

            if len(total_sentiment.items()) == 0:
                total_sentiment = sentiment
            else:
                total_sentiment = self.__update_dic(total_sentiment, sentiment)
            if len(total_emotion.items()) == 0:
                total_emotion = emotions
            else:
                total_emotion = self.__update_dic(total_emotion, emotions)
            new_trend[date] = (count, profanity, retweet)
            trend_sentiment[date] = sentiment
            trend_emotion[date] = emotions

        for item in total_emotion:
            total_emotion[item] /= len(trend.items())
        for item in total_sentiment:
            total_sentiment[item] /= len(trend.items())

        t_emotion = 0
        for item in total_emotion:
            t_emotion += total_emotion[item]
        t_sentiment = 0
        for item in total_sentiment:
            t_sentiment += total_sentiment[item]

        for item in total_emotion:
            if t_emotion != 0:
                total_emotion[item] /= t_emotion
        for item in total_sentiment:
            if t_sentiment != 0:
                total_sentiment[item] /= t_sentiment

        new_trend = sorted(new_trend.items(), key=operator.itemgetter(0))
        trend_sentiment = sorted(trend_sentiment.items(), key=operator.itemgetter(0))
        trend_emotion = sorted(trend_emotion.items(), key=operator.itemgetter(0))
        return new_trend, trend_sentiment, trend_emotion, total_sentiment, total_emotion

    def __extract_twitter_influencers(self):
        result = {}
        # for c in category:
        #     for t in topics:
        for item in self.influencers:
            # item_category = item['category']
            # item_topic = item['topic']
            item_source = item['source']
            if item_source == 'tweets':
                for inf in item['influencers']:
                    author = inf['author']
                    likesAndRetweets = int(inf['tweets'])
                    url = inf['urls'][-1]
                    if author not in result:
                        result[author] = (likesAndRetweets, url)
                    else:
                        likes = result[author][0]
                        url = result[author][1]
                        result[author] = (likes + likesAndRetweets, url)
        if len(result.items()) != 0:
            result = sorted(result.items(), key=lambda x: x[1][0])
            result.reverse()
        else:
            result = []
        if len(result) > 100:
            result = result[:100]
        return result

    def __extract_telegram_influencers(self):
        result = {}
        # for c in category:
        # for t in topics:
        for item in self.influencers:
            # item_category = item['category']
            # item_topic = item['topic']
            item_source = item['source']
            if item_source == 'telegram':
                for inf in item['influencers']:
                    name = inf['channel']
                    posts = inf['posts']
                    views = inf['view']
                    avg = views / posts
                    if name not in result:
                        result[name] = (posts, views, avg)
                    else:
                        p = result[name][0]
                        v = result[name][1]
                        a = result[name][2]
                        result[name] = (p + posts, v + views, (v + views) / (p + posts))
        if len(result.items()) != 0:
            result = sorted(result.items(), key=lambda x: x[1][0])
            result.reverse()
        else:
            result = []
        if len(result) > 100:
            result = result[:100]
        return result

    def __extract_news_influencers(self):
        result = {}
        # for c in category:
        # for t in topics:
        for item in self.influencers:
            # item_category = item['category']
            # item_topic = item['topic']
            item_source = item['source']
            if item_source == 'news':
                for inf in item['influencers']:
                    domain = inf['domain']
                    count = inf['count']
                    if domain not in result:
                        result[domain] = count
                    else:
                        result[domain] += count
        if len(result.items()) != 0:
            result = sorted(result.items(), key=lambda x: x[1])
            result.reverse()
        else:
            result = []
        if len(result) > 100:
            result = result[:100]
        return result

    def __add_tuples_to_dic(self, tuples, dic):
        for t in tuples:
            name = t[0]
            name = name.strip()
            name = name.replace(',', '')
            count = t[1]
            if name != '':
                if name not in dic:
                    dic[name] = count
                else:
                    dic[name] += count
        return dic

    # def __extract_associations(self, source, category, topics):
    #     result = {}
    #     events = {}
    #     locations = {}
    #     organizations = {}
    #     for item in self.associations:
    #         item_source = item['source']
    #         item_category = item['category']
    #         item_topic = item['topic']
    #         if item_source == source and item_category in category and item_topic in topics:
    #             item_associations = item['entities']
    #             item_events = item_associations['events']
    #             item_locations = item_associations['locations']
    #             item_organizations = item_associations['organizations']
    #             events = self.__add_tuples_to_dic(item_events, events)
    #             locations = self.__add_tuples_to_dic(item_locations, locations)
    #             organizations = self.__add_tuples_to_dic(item_organizations, organizations)
    #     if len(events.items()) > 0:
    #         events = sorted(events.items(), key=operator.itemgetter(1))
    #         events.reverse()
    #     else:
    #         events = []
    #     if len(locations.items()) > 0:
    #         locations = sorted(locations.items(), key=operator.itemgetter(1))
    #         locations.reverse()
    #     else:
    #         locations = []
    #     if len(organizations.items()) > 0:
    #         organizations = sorted(organizations.items(), key=operator.itemgetter(1))
    #         organizations.reverse()
    #     else:
    #         organizations = []
    #     if len(events) > 30:
    #         events = events[:30]
    #     if len(locations) > 30:
    #         locations = locations[:30]
    #     if len(organizations) > 30:
    #         organizations = organizations[:30]
    #     result['events'] = events
    #     result['locations'] = locations
    #     result['organizations'] = organizations
    #     return result

    def __topic_aggregation(self, source: str, start: str, end: str):
        aggregated_topics = {}
        if source == 'news':
            items = self.news
            for item in self.news_topics:
                aggregated_topics[item] = 0
        elif source == 'telegram':
            items = self.posts
            for item in self.telegram_topics:
                aggregated_topics[item] = 0
        # else:
        #     items = self.tweets
        #     for item in self.twitter_topics:
        #         aggregated_topics[item] = 0
        for item in items:
            # cat = item['category']
            date = item['date']
            # if self.__search(category, cat):
            if start == '':
                start_cond = 0
            else:
                start_cond = self.__compare_dates(date, start)
            if end == '':
                end_cond = 0
            else:
                end_cond = self.__compare_dates(date, end)
            if (start_cond == 1 or start_cond == 0) and (end_cond == -1 or end_cond == 0):
                topics = item['topics']
                for t in topics:
                    aggregated_topics[t] += topics[t]
        total = 0
        for t in aggregated_topics:
            total += aggregated_topics[t]
        for t in aggregated_topics:
            if total != 0:
                aggregated_topics[t] /= total
        return aggregated_topics

    def __compare_dates(self, date_1: str, date_2: str):
        tokens_1 = date_1.split('-')
        tokens_2 = date_2.split('-')
        if len(tokens_1) != 3 or len(tokens_2) != 3:
            return -2
        else:
            for index in range(3):
                if int(tokens_1[index]) < int(tokens_2[index]):
                    return -1
                elif int(tokens_1[index]) > int(tokens_2[index]):
                    return 1
        return 0

    def data_statistics(self, query, source: str, topics: list, start='', end=''):
        self.news, self.posts, self.tweets, self.influencers = self.load_files(query)
        # if len(category) == 0:
        #     category = self.categories
        if len(topics) == 0:
            if source == 'news':
                topics = self.news_topics
            elif source == 'telegram':
                topics = self.telegram_topics
            # elif source == 'twitter':
            #     topics = self.twitter_topics
        stats = {}
        filtered = []
        if True or self.__check_parameters(source, topics):
            if source == 'news':
                filtered, agg_topics = self.__filter('news', topics, start, end)
            elif source == 'telegram':
                filtered, agg_topics = self.__filter('telegram', topics, start, end)
            elif source == 'twitter':
                filtered, agg_topics = self.tweets, None

            trend = []
            if source == 'news' or source == 'telegram':
                trend = self.__unify_on_dates(filtered)
            elif source == 'twitter':
                trend, trend_sentiment, trend_emotion, avg_sentiment, avg_emotion = self.__unify_on_dates_for_twitter(self.tweets)
                stats['sentiment_trend'] = trend_sentiment
                stats['emotion_trend'] = trend_emotion
                stats['average_sentiment'] = avg_sentiment
                stats['average_emotion'] = avg_emotion
            stats['trend'] = trend

            influencers = []
            if source == 'twitter':
                influencers = self.__extract_twitter_influencers()
            elif source == 'news':
                influencers = self.__extract_news_influencers()
            elif source == 'telegram':
                influencers = self.__extract_telegram_influencers()
            stats['influencers'] = influencers

            # if source == 'news' or source == 'telegram':
            #     stats['associations'] = self.__extract_associations(source, category, topics)

            stats['topic_aggregation'] = agg_topics

        return stats

    # def event_statistics(self, event_date: str, start_date: str, end_date: str):
    #     news_items_before, news_agg_topics_before = self.__filter('news', self.categories, self.news_topics, start_date,
    #                                                               event_date)
    #     news_items_after, news_agg_topics_after = self.__filter('news', self.categories, self.news_topics, event_date,
    #                                                             end_date)
    #
    #     telegram_items_before, telegram_agg_topics_before = self.__filter('telegram', self.categories,
    #                                                                       self.telegram_topics, start_date, event_date)
    #     telegram_items_after, telegram_agg_topics_after = self.__filter('telegram', self.categories,
    #                                                                       self.telegram_topics, event_date, end_date)
    #
    #     twitter_items_before, twitter_agg_topics_before = self.__filter('twitter', self.categories,
    #                                                                       self.twitter_topics, start_date, event_date)
    #     twitter_items_after, twitter_agg_topics_after = self.__filter('twitter', self.categories,
    #                                                                     self.twitter_topics, event_date, end_date)
    #
    #     result = {}
    #     topics_result = dict()
    #     topics_result['news'] = {'before': news_agg_topics_before, 'after': news_agg_topics_after}
    #     topics_result['telegram'] = {'before': telegram_agg_topics_before, 'after': telegram_agg_topics_after}
    #     topics_result['twitter'] = {'before': twitter_agg_topics_before, 'after': twitter_agg_topics_after}
    #     result['topics'] = topics_result
    #
    #     twitter_trend_before, trend_sentiment_before, trend_emotion_before, avg_sentiment_before, avg_emotion_before = self.__unify_on_dates_for_twitter(twitter_items_before)
    #     twitter_trend_after, trend_sentiment_after, trend_emotion_after, avg_sentiment_after, avg_emotion_after = self.__unify_on_dates_for_twitter(twitter_items_after)
    #
    #     sentiment_result = {'before': avg_sentiment_before, 'after': avg_sentiment_after}
    #     emotion_result = {'before': avg_emotion_before, 'after': avg_emotion_after}
    #
    #     result['sentiment'] = sentiment_result
    #     result['emotion'] = emotion_result
    #     return result
