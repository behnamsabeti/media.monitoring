# -*- coding: utf-8 -*-

import plotly.graph_objects as go
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import numpy as np
from vision.squarifyData import SquarifyChart
from vision.app import app
from vision.data_exploration import DataExploration
import operator
from dash.exceptions import PreventUpdate


# start_time = '2019-04-25'
# end_time = '2019-10-03'
    
data_model = DataExploration()


    
def CreateTopicList(topicList):
    topics = []
    for topic in topicList:
        topics.append({'label':topic, 'value':topic})
    return topics



# totalSnappDataframeData = []
# result_news_snapp = data_model.data_statistics('elections', 'news', [],start_time, end_time)
#
# topicsList = list(result_news['topic_aggregation'].keys())
# topicOptionList = CreateTopicList(topicsList)
#
# x = result_news_snapp['topic_aggregation']
# sorted_x = sorted(x.items(), key=operator.itemgetter(1))
# data_label = []
# data_value = []
# for item in sorted_x:
#     if(item[1] != 0):
#         data_label.append(item[0])
#         data_value.append(item[1])



layout = html.Div([
    html.H1('News'),
    html.Div([
        html.P('Query Name'),
        dcc.Dropdown(
            id='news-query-dropdown',
            options=[{'label': l, 'value': l} for l in data_model.query_names()
                     ],
            value=None,
        )
    ], className='Width50Percent FloatLeft'),
    dcc.Graph(
        id='news-topic-graph',
        figure={},
        config={
                'displayModeBar': False
            },
        className='FullWidth FloatLeft'
    ),
    # html.Div([html.P('Topic')],className='FullWidth FloatLeft'),
    # html.Div(id = 'news-dropdown-div',children=[
    #     dcc.Dropdown(
    #         id='news-dropdown',
    #         options=topicOptionList,
    #         value=data_label,
    #         multi=True,
    #         clearable=False
    #                 )
    #         ],className='Width50Percent FloatLeft'),
    # html.Div([
    #     html.Button('Submit', id='news-submit-button', className='ButtonStyle')
    #     ], className='Width50Percent FloatLeft'),
    # html.Div([
    #     dcc.Dropdown(
    #         id='news-event-dropdown',
    #         options=[
    #                 {'label': 'Snapp Hijab Crisis', 'value':'2019-06-06'}
    #                 ],
    #         placeholder='Choose an event'
    #                 )
    #         ],className='Width50Percent FloatLeft MarginRight5Percent MarginTop40Px'),
    html.Div([
            html.Div([
                    dcc.Graph(
                            id='news-total-trend',
                            figure={}
                            )
                ],className='RowDivStyle'),
            # html.Div([
            #     dcc.Graph(
            #         id='news-association-graph',
            #         figure={},
            #         config={
            #                 'displayModeBar': False
            #             },
            #     ),
            #     ],className='RowDivStyle'),
            html.Div([
                    html.Div(id='influencer-segment-news',children=[
                            ], className='InfluencerListSegmentDivStyle'),
                    html.Div([
                            html.Div([
                                    html.P('Total News that are Published', className='TotalTwitteTextLabel'),
                                    html.P(id='totalNewsCount',children='', className='TotalTwitteTextValue'),
                                    ], className='InfluencerDataTotalDivStyle'),
                            html.Div([
                                    html.P('Top 100 News websites Published', className='TotalTwitteTextLabel'),
                                    html.P(id='totalInfluencerNewsCount',children='', className='TotalTwitteTextValue'),
                                    ], className='NewsInfluencerDataTotalInfluencerDivStyle')
                            ], className='InfluencerDataAnalysisDivStyle'),
            ], className='RowDivStyle'),
        html.Div([
                dcc.Graph(
                    id='news-influencer-cumulative-graph',
                    figure={}
                ),
                ],className='RowDivStyle MarginBottom40px'),
        html.Div([
                html.Div([html.Span(id='firstQuartileCount', style={'fontSize':'2vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),'WebSite Generate',html.Span('  25% ', style={'fontSize':'1.3vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),' of news'], className='StatisticBoxLeft'),
                html.Div([html.Span(id='secondQuartileCount', style={'fontSize':'2vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),'WebSite Generate',html.Span('  50% ', style={'fontSize':'1.3vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),' of news'], className='StatisticBoxMiddleRight'),
                html.Div([html.Span(id='thirdQuartileCount', style={'fontSize':'2vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),'WebSite Generate',html.Span('  75% ', style={'fontSize':'1.3vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),' of news'], className='StatisticBoxMiddleRight'),
                ],className='RowDivStyle MarginBottom40px')
        ],className='TwitterVisBodyStyle')
    ], className='LandingBodyDivStyle')

# @app.callback(dash.dependencies.Output('news-dropdown-div', 'children'),
#               [dash.dependencies.Input('news-dropdown', 'value')],
#               [dash.dependencies.State('news-dropdown-div', 'children')])
# def topicDropDownUpdate(valueTopic,prevDivChild):
#     if(prevDivChild[0]['props']['value'] == valueTopic):
#         raise PreventUpdate
#     if(valueTopic == []):
#         valueTopic = data_label
#     dropdown = dcc.Dropdown(
#         id='news-dropdown',
#         options=topicOptionList,
#         value=valueTopic,
#         multi=True,
#         clearable=False
#         )
#     return dropdown
#
#
# @app.callback(dash.dependencies.Output('news-topic-graph', 'figure'),
#               [dash.dependencies.Input('news-category-dropdown', 'value')])
# def topicChartUpdate(valueCategory):
#     result_news_category = data_model.data_statistics('news', valueCategory, [],start_time, end_time)
#     x = result_news_category['topic_aggregation']
#     sorted_x = sorted(x.items(), key=operator.itemgetter(1))
#     data_label = []
#     data_value = []
#     for item in sorted_x:
#         if(item[1] != 0):
#             data_label.append(item[0])
#             data_value.append(item[1])
#     fig = SquarifyChart(200,100,data_value,data_label,1400,700,7,'News Topic Frequency')
#     return fig
             


@app.callback([dash.dependencies.Output('news-total-trend', 'figure'),
               # dash.dependencies.Output('news-association-graph', 'figure'),
               dash.dependencies.Output('influencer-segment-news', 'children'),
               dash.dependencies.Output('totalNewsCount', 'children'),
               dash.dependencies.Output('totalInfluencerNewsCount', 'children'),
               dash.dependencies.Output('news-influencer-cumulative-graph', 'figure'),
               dash.dependencies.Output('firstQuartileCount', 'children'),
               dash.dependencies.Output('secondQuartileCount', 'children'),
               dash.dependencies.Output('thirdQuartileCount', 'children'),
               dash.dependencies.Output('news-topic-graph', 'figure'),
               dash.dependencies.Output('news-query-dropdown', 'options'),],
              [dash.dependencies.Input('news-query-dropdown', 'value'),])
               # dash.dependencies.Input('news-category-dropdown', 'value'),
               # dash.dependencies.Input('news-event-dropdown', 'value')],
               # [dash.dependencies.State('news-dropdown', 'value')])
def trendChartUpdate(query):
    options = [{'label': l, 'value': l} for l in data_model.query_names()]
    if query is not None:
        result_news = data_model.data_statistics(query, 'news', [])
        totalTrendDataframeData_temp = []
        for item in result_news['trend']:
            totalTrendDataframeData_temp.append([item[0]])
        totalTrend = pd.DataFrame(data=totalTrendDataframeData_temp, columns=['Time'])
        trend_filterCategory_allTopics_dataframe_temp_temp = []
        result_news_filterCategory_allTopic_temp = data_model.data_statistics(query, 'news', [])
        for item in result_news_filterCategory_allTopic_temp['trend']:
            trend_filterCategory_allTopics_dataframe_temp_temp.append([item[0],item[1]])
        trend_filterCategory_allTopics_dataframe_temp_ = pd.DataFrame(data=trend_filterCategory_allTopics_dataframe_temp_temp, columns=['Time','Value'])
        total_trend_filterCategory_allTopics_ = totalTrend.merge(trend_filterCategory_allTopics_dataframe_temp_, how='outer', on=['Time'])
        total_trend_filterCategory_allTopics_ = total_trend_filterCategory_allTopics_.fillna(0)
        
        
        total_trend_filterCategory_allTopics__time = total_trend_filterCategory_allTopics_['Time'].values.tolist()
        total_trend_filterCategory_allTopics__value = total_trend_filterCategory_allTopics_['Value'].values.tolist()
        
        trend_filterCategory_filterTopics_dataframe_temp = []
        result_news_filterCategory_filterTopic = data_model.data_statistics(query, 'news', [])
        for item in result_news_filterCategory_filterTopic['trend']:
            trend_filterCategory_filterTopics_dataframe_temp.append([item[0],item[1]])
        trend_filterCategory_filterTopics_dataframe = pd.DataFrame(data=trend_filterCategory_filterTopics_dataframe_temp, columns=['Time','Value'])
        total_trend_filterCategory_filterTopics = totalTrend.merge(trend_filterCategory_filterTopics_dataframe, how='outer', on=['Time'])
        total_trend_filterCategory_filterTopics = total_trend_filterCategory_filterTopics.fillna(0)
        
        
        total_trend_filterCategory_filterTopics__value = total_trend_filterCategory_filterTopics['Value'].values.tolist()
        
        total_filterCategory_alltopics_data_count = np.array(total_trend_filterCategory_allTopics__value)
        total_filterCategory_filtertopics_data_count = np.array(total_trend_filterCategory_filterTopics__value)
        new_totaly = list(total_filterCategory_alltopics_data_count - total_filterCategory_filtertopics_data_count)
        
        
        # news_association_filterCategory_filterTopic = result_news_filterCategory_filterTopic['associations']
        # assoc_fig_labels = []
        # assoc_fig_parent = []
        # assoc_fig_value = []
        # assoc_fig_labels.append('Association')
        # assoc_fig_parent.append('')
        # assoc_fig_value.append(100)
        
        # for ind,item in enumerate(list(news_association_filterCategory_filterTopic.keys())):
        #     fraction = round(100/len(news_association_filterCategory_filterTopic),3)
        #     if((ind + 1) < len(list(news_association_filterCategory_filterTopic.keys()))):
        #         assoc_fig_labels.append(item)
        #         assoc_fig_parent.append('Association')
        #         assoc_fig_value.append(fraction)
        #         tempData = news_association_filterCategory_filterTopic[item]
        #         totalSum = sum([pair[1] for pair in tempData])
        #         temp_sum = 0
        #         for ind2,pair in enumerate(tempData):
        #             fraction2 = round((pair[1]*fraction)/totalSum,3)
        #             if((ind2 +1 ) < len(tempData)):
        #                 temp_sum += fraction2
        #                 assoc_fig_labels.append(pair[0])
        #                 assoc_fig_parent.append(item)
        #                 assoc_fig_value.append(fraction2)
        #             else:
        #                 assoc_fig_labels.append(pair[0])
        #                 assoc_fig_parent.append(item)
        #                 assoc_fig_value.append(round(fraction - round(temp_sum,3),3)-0.001)
        #     else:
        #         fraction_final = round((100 - (fraction*(len(list(news_association_filterCategory_filterTopic.keys())) - 1))),3)
        #         assoc_fig_labels.append(item)
        #         assoc_fig_parent.append('Association')
        #         assoc_fig_value.append(fraction_final)
        #         tempData = news_association_filterCategory_filterTopic[item]
        #         totalSum = sum([pair[1] for pair in tempData])
        #         temp_sum = 0
        #         for ind2,pair in enumerate(tempData):
        #             fraction3 = round((pair[1]*fraction_final)/totalSum,3)
        #             if((ind2+1) < len(tempData)):
        #                 temp_sum += fraction3
        #                 assoc_fig_labels.append(pair[0])
        #                 assoc_fig_parent.append(item)
        #                 assoc_fig_value.append(fraction3)
        #             else:
        #                 assoc_fig_labels.append(pair[0])
        #                 assoc_fig_parent.append(item)
        #                 assoc_fig_value.append(round(fraction_final - round(temp_sum,3),3)-0.001)
        #
        #
        # fig_association_news =go.Figure(go.Sunburst(
        #         labels=assoc_fig_labels,
        #         parents=assoc_fig_parent,
        #         values=assoc_fig_value,
        #         branchvalues="total",
        #         ))
        # fig_association_news.update_layout(margin = dict(t=20, l=0, r=0, b=40), height=700)


        influencer_list = result_news_filterCategory_filterTopic['influencers']
        influencerDivChild = []
        maxInfCount = influencer_list[0][1]
        minInfCount = influencer_list[-1][1]
        tot_inf_news_count = 0
        influencerName = []
        influencercumulative = []
        firstQuartileValue = int(sum(total_trend_filterCategory_filterTopics__value)/4)
        secondQuartileValue = int(sum(total_trend_filterCategory_filterTopics__value)/2)
        thirdQuartileValue = int(sum(total_trend_filterCategory_filterTopics__value)*3/4)
        firstQuartileFlag = 0
        secondQuartileFlag = 0
        thirdQuartileFlag = 0
        
        firstQuartileIndex = -1 
        secondQuartileIndex = -1
        thirdQuartileIndex = -1
        
        for i,inf in enumerate(influencer_list):
            influencerName.append(inf[0])
            tot_inf_news_count += inf[1]
            if(tot_inf_news_count >= firstQuartileValue and firstQuartileFlag == 0):
                firstQuartileIndex = i
                firstQuartileFlag = 1
            if(tot_inf_news_count >= secondQuartileValue and secondQuartileFlag == 0):
                secondQuartileIndex = i
                secondQuartileFlag = 1
            if(tot_inf_news_count >= thirdQuartileValue and thirdQuartileFlag == 0):
                thirdQuartileIndex = i
                thirdQuartileFlag = 1
            influencercumulative.append(tot_inf_news_count)
            changeScale = (((inf[1] - minInfCount) * 60)/(maxInfCount - minInfCount)) + 40
            barWidth = str(int(changeScale)) + '%'
            inf_layout = html.Div([
                    html.Div([
                            html.Div([
                                    html.P(str(inf[0]),style={'float':'left', 'marginLeft':'2%', 'fontSize':14,'marginTop':5}),
                                    html.P(str(inf[1]),style={'float':'right', 'marginRight':'2%', 'marginTop':5,'fontSize':14})
                                    ],style={'width':barWidth, 'background':'#6adddd', 'height':30, 'minHeight':1})
                            ], className='InfluencerBarChartDivStyle'),
                    ],className='FullWidth FloatLeft MarginTop20Px')
            influencerDivChild.append(inf_layout)
        influencerName.append('Other')
        influencercumulative.append(int(sum(total_trend_filterCategory_filterTopics__value)))
        
        firstQString = str(firstQuartileIndex + 1) + '  '
        secondQString = str(secondQuartileIndex + 1) + '  '
        thirdQString = str(thirdQuartileIndex + 1) + '  '
        
        if(firstQuartileIndex == -1):########
            firstQString = '> 100  '
        if(secondQuartileIndex == -1):########
            secondQString = '> 100  '
        if(thirdQuartileIndex == -1):########
            thirdQString = '> 100  '
        
        dictNewsInfluencerCumulative = {
                    'data': [
                            go.Bar(
                                    x=influencerName,
                                    y=influencercumulative,
                                    marker=go.bar.Marker(
                                            color='rgb(60,124,181)',
                                            )
                                    ),
                            go.Scatter(x=[influencerName[0],influencerName[thirdQuartileIndex]], 
                                       y=[thirdQuartileValue,thirdQuartileValue], 
                                       mode='lines+text',
                                       line=dict(width=2, color='#D1CE42'), 
                                       fill='tozeroy',
                                       fillcolor='rgba(209, 206, 66, 0.4)',
                                       text=['Third Quartile',''], 
                                       textposition='top right'),
                            go.Scatter(x=[influencerName[0],influencerName[secondQuartileIndex]], 
                                       y=[secondQuartileValue,secondQuartileValue], 
                                       mode='lines+text',
                                       line=dict(width=2, color='#45D3D3'), 
                                       fill='tozeroy',
                                       fillcolor='rgba(69, 211, 211, 0.2)',
                                       text=['Second Quartile',''], 
                                       textposition='top right'),
                            go.Scatter(x=[influencerName[0],influencerName[firstQuartileIndex]], 
                                       y=[firstQuartileValue,firstQuartileValue], 
                                       mode='lines+text',
                                       line=dict(width=2, color='#E92910'),
                                       fill='tozeroy',
                                       fillcolor='rgba(233, 41, 16, 0.3)', 
                                       text=['First Quartile',''], 
                                       textposition='top right')
                            
                            ],
                            'layout': go.Layout(
                                    showlegend=False,
                                    title=dict(text='News Frequency Cumulative Graph'),
                                    height=600,
                                    margin=dict(b=180, l=80),
                                    annotations=[
                                        go.layout.Annotation(
                                            x=0.5,
                                            y=-0.55,
                                            showarrow=False,
                                            text="News Website",
                                            font=dict(size=18),
                                            xref="paper",
                                            yref="paper"
                                        )],
                                    yaxis=dict(title=dict(text='Cumulative News Count')),
                                    )                
                }
        eventLineMaxValue = max(total_trend_filterCategory_allTopics__value)+50
        # if(eventValue != None):
        #     dictTotalTrend = {
        #             'data': [
        #                     go.Bar(
        #                             x=total_trend_filterCategory_allTopics__time,
        #                             y=total_trend_filterCategory_filterTopics__value,
        #                             name='Selected Topic',
        #                             marker=go.bar.Marker(
        #                                     color='rgb(60,124,181)',
        #                                     )
        #                             ),
        #                     go.Bar(
        #                             x=total_trend_filterCategory_allTopics__time,
        #                             y=new_totaly,
        #                             name='total',
        #                             marker=go.bar.Marker(
        #                                     color='rgb(205,231,241)',
        #                                     )
        #                             ),
        #                     go.Scatter(x=[eventValue,eventValue], y=[0,eventLineMaxValue], mode='lines',line=dict(width=2, color='red'))
        #                     ],
        #                     'layout': go.Layout(
        #                             showlegend=True,
        #                             barmode='stack',
        #                             xaxis=dict(title=dict(text='Time')),
        #                             yaxis=dict(title=dict(text='News Count')),
        #                             )
        #             }
        if True:
            dictTotalTrend = {
                    'data': [
                            go.Bar(
                                    x=total_trend_filterCategory_allTopics__time,
                                    y=total_trend_filterCategory_filterTopics__value,
                                    name='Selected Topic',
                                    marker=go.bar.Marker(
                                            color='rgb(60,124,181)',
                                            )
                                    ),
                            go.Bar(
                                    x=total_trend_filterCategory_allTopics__time,
                                    y=new_totaly,
                                    name='total',
                                    marker=go.bar.Marker(
                                            color='rgb(205,231,241)',
                                            )
                                    )
                            ],
                            'layout': go.Layout(
                                    showlegend=True,
                                    barmode='stack',
                                    xaxis=dict(title=dict(text='Time')),
                                    yaxis=dict(title=dict(text='News Count')),
                                    )
                    }

        x = result_news_filterCategory_filterTopic['topic_aggregation']
        sorted_x = sorted(x.items(), key=operator.itemgetter(1))
        data_label = []
        data_value = []
        for item in sorted_x:
            if(item[1] > 0.05):
                data_label.append(item[0])
                data_value.append(item[1])
        fig = SquarifyChart(200,100,data_value,data_label,1400,700,7,'News Topic Frequency')
                                
        return dictTotalTrend,influencerDivChild,format(int(sum(total_trend_filterCategory_filterTopics__value)),',d'),format(tot_inf_news_count,',d'),dictNewsInfluencerCumulative,firstQString,secondQString,thirdQString, fig, options
    else:
        return {},[],'','',{},'','','', {}, options
