# -*- coding: utf-8 -*-

import plotly.graph_objects as go
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import numpy as np
from vision.squarifyData import SquarifyChart
from vision.app import app
from vision.data_exploration import DataExploration
import operator
from dash.exceptions import PreventUpdate
from plotly.subplots import make_subplots
import time


# start_time = '2018-06-03'
# end_time = '2019-10-03'
    
data_model = DataExploration()


    
def CreateTopicList(topicList):
    topics = []
    for topic in topicList:
        topics.append({'label':topic, 'value':topic})
    return topics



# totalSnappDataframeData = []
# result_telegram_snapp = data_model.data_statistics('elections', 'telegram', [])

# topicsList = list(result_telegram['topic_aggregation'].keys())
# topicOptionList = CreateTopicList(topicsList)

# x = result_telegram_snapp['topic_aggregation']
# sorted_x = sorted(x.items(), key=operator.itemgetter(1))
# data_label = []
# data_value = []
# for item in sorted_x:
#     if(item[1] != 0):
#         data_label.append(item[0])
#         data_value.append(item[1])



layout = html.Div([
    html.H1('Telegram'),
    html.Div([
        html.P('Query Name'),
        dcc.Dropdown(
            id='telegram-query-dropdown',
            options=[{'label': l, 'value': l} for l in data_model.query_names()
                     ],
            value=None,
        )
    ], className='Width50Percent FloatLeft'),
    dcc.Graph(
        id='telegram-topic-graph',
        figure={},
        config={
                'displayModeBar': False
            },
        className='FullWidth FloatLeft'
    ),
    # html.Div([html.P('Topic')],className='FullWidth FloatLeft'),
    # html.Div(id = 'telegram-dropdown-div',children=[
    #     dcc.Dropdown(
    #         id='telegram-dropdown',
    #         options=topicOptionList,
    #         value=data_label,
    #         multi=True,
    #         clearable=False
    #                 )
    #         ],className='Width50Percent FloatLeft'),
    # html.Div([
    #     html.Button('Submit', id='telegram-submit-button', className='ButtonStyle')
    #     ], className='Width50Percent FloatLeft'),
    # html.Div([
    #     dcc.Dropdown(
    #         id='telegram-event-dropdown',
    #         options=[
    #                 {'label': 'Snapp Hijab Crisis', 'value':'2019-06-06'},
    #                 {'label': 'Saman investment package', 'value':'2019-03-05'},
    #                 {'label': 'Snapp super App lunch', 'value':'2018-07-15'}
    #                 ],
    #         placeholder='Choose an event'
    #                 )
    #         ],className='Width50Percent FloatLeft MarginRight5Percent MarginTop40Px'),
    html.Div([
            html.Div([
                    dcc.Graph(
                            id='telegram-total-trend',
                            figure={}
                            )
                ],className='RowDivStyle'),
            # html.Div([
            #     dcc.Graph(
            #         id='telegram-association-graph',
            #         figure={},
            #         config={
            #                 'displayModeBar': False
            #             },
            #     ),
            #     ],className='RowDivStyle'),
    html.Div([
            html.Div(id='influencer-segment-telegram',children=[
                    ], className='InfluencerListSegmentDivStyle'),
            html.Div([
                    html.Div([
                            html.P('Total Telegram Post', className='TotalTwitteTextLabel'),
                            html.P(id='totalTelegramCount',children='', className='TotalTwitteTextValue'),
                            ], className='InfluencerDataTotalDivStyle'),
                    html.Div([
                            html.P('Top 100 Channel Telegram Post', className='TotalTwitteTextLabel'),
                            html.P(id='totalInfluencerTelegramCount',children='', className='TotalTwitteTextValue'),
                            ], className='NewsInfluencerDataTotalInfluencerDivStyle')
                    ], className='InfluencerDataAnalysisDivStyle'),
            ], className='RowDivStyle'),
    html.Div([
            dcc.Graph(
                    id='telegram-post-count-graph',
                    figure={}
                ),
                ],className='RowDivStyle MarginBottom40px'),
    html.Div([
            html.Div([html.Span(id='telegramfirstQuartileCount', style={'fontSize':'2vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),'Channels Generate',html.Span('  25% ', style={'fontSize':'1.3vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),' of Posts'], className='StatisticBoxLeft'),
            html.Div([html.Span(id='telegramsecondQuartileCount', style={'fontSize':'2vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),'Channels Generate',html.Span('  50% ', style={'fontSize':'1.3vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),' of Posts'], className='StatisticBoxMiddleRight'),
            html.Div([html.Span(id='telegramthirdQuartileCount', style={'fontSize':'2vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),'Channels Generate',html.Span('  75% ', style={'fontSize':'1.3vw', 'color':'rgb(76,153,160)', 'fontWeight':'bold'}),' of Posts'], className='StatisticBoxMiddleRight'),
            ],className='RowDivStyle MarginBottom40px'),
    html.Div(id='telegram-inf-graph', children=[
                ],className='RowDivStyle MarginBottom40px')
        ],className='TwitterVisBodyStyle')
    ], className='LandingBodyDivStyle')

# @app.callback(dash.dependencies.Output('telegram-dropdown-div', 'children'),
#               [dash.dependencies.Input('telegram-dropdown', 'value')],
#               [dash.dependencies.State('telegram-dropdown-div', 'children')])
# def topicDropDownUpdate(valueTopic,prevDivChild):
#     if(prevDivChild[0]['props']['value'] == valueTopic):
#         raise PreventUpdate
#     if(valueTopic == []):
#         valueTopic = data_label
#     dropdown = dcc.Dropdown(
#         id='telegram-dropdown',
#         options=topicOptionList,
#         value=valueTopic,
#         multi=True,
#         clearable=False
#         )
#     return dropdown

        
# @app.callback(dash.dependencies.Output('telegram-topic-graph', 'figure'),
#               [dash.dependencies.Input('telegram-category-dropdown', 'value')])
# def topicChartUpdate(valueCategory):
#     result_telegram_category = data_model.data_statistics('telegram', valueCategory, [],start_time, end_time)
#     x = result_telegram_category['topic_aggregation']
#     sorted_x = sorted(x.items(), key=operator.itemgetter(1))
#     data_label = []
#     data_value = []
#     for item in sorted_x:
#         if(item[1] != 0):
#             data_label.append(item[0])
#             data_value.append(item[1])
#     fig = SquarifyChart(200,100,data_value,data_label,1400,700,9,'Telegram Topic Frequency')
#     return fig
             


@app.callback([dash.dependencies.Output('telegram-total-trend', 'figure'),
               # dash.dependencies.Output('telegram-association-graph', 'figure'),
               dash.dependencies.Output('influencer-segment-telegram', 'children'),
               dash.dependencies.Output('totalTelegramCount', 'children'),
               dash.dependencies.Output('totalInfluencerTelegramCount', 'children'),
               dash.dependencies.Output('telegram-post-count-graph', 'figure'),
               dash.dependencies.Output('telegramfirstQuartileCount', 'children'),
               dash.dependencies.Output('telegramsecondQuartileCount', 'children'),
               dash.dependencies.Output('telegramthirdQuartileCount', 'children'),
               dash.dependencies.Output('telegram-inf-graph', 'children'),
               dash.dependencies.Output('telegram-topic-graph', 'figure'),
               dash.dependencies.Output('telegram-query-dropdown', 'options'),],
              [dash.dependencies.Input('telegram-query-dropdown', 'value')])
               # dash.dependencies.Input('telegram-category-dropdown', 'value'),
               # dash.dependencies.Input('telegram-event-dropdown', 'value')],
               # [dash.dependencies.State('telegram-dropdown', 'value')])
def trendChartUpdate(query):
    options = [{'label': l, 'value': l} for l in data_model.query_names()]
    if query is not None:
        result_telegram = data_model.data_statistics(query, 'telegram', [])
        totalTrendDataframeData_temp = []
        for item in result_telegram['trend']:
            totalTrendDataframeData_temp.append([item[0]])
        totalTrend = pd.DataFrame(data=totalTrendDataframeData_temp, columns=['Time'])
        trend_filterCategory_allTopics_dataframe_temp_temp = []
        result_telegram_filterCategory_allTopic_temp = data_model.data_statistics(query, 'telegram', [])
        for item in result_telegram_filterCategory_allTopic_temp['trend']:
            trend_filterCategory_allTopics_dataframe_temp_temp.append([item[0],item[1]])
        trend_filterCategory_allTopics_dataframe_temp_ = pd.DataFrame(data=trend_filterCategory_allTopics_dataframe_temp_temp, columns=['Time','Value'])
        total_trend_filterCategory_allTopics_ = totalTrend.merge(trend_filterCategory_allTopics_dataframe_temp_, how='outer', on=['Time'])
        total_trend_filterCategory_allTopics_ = total_trend_filterCategory_allTopics_.fillna(0)
        
        
        total_trend_filterCategory_allTopics__time = total_trend_filterCategory_allTopics_['Time'].values.tolist()
        total_trend_filterCategory_allTopics__value = total_trend_filterCategory_allTopics_['Value'].values.tolist()
        
        trend_filterCategory_filterTopics_dataframe_temp = []
        result_telegram_filterCategory_filterTopic = data_model.data_statistics(query, 'telegram', [])
        for item in result_telegram_filterCategory_filterTopic['trend']:
            trend_filterCategory_filterTopics_dataframe_temp.append([item[0],item[1]])
        trend_filterCategory_filterTopics_dataframe = pd.DataFrame(data=trend_filterCategory_filterTopics_dataframe_temp, columns=['Time','Value'])
        total_trend_filterCategory_filterTopics = totalTrend.merge(trend_filterCategory_filterTopics_dataframe, how='outer', on=['Time'])
        total_trend_filterCategory_filterTopics = total_trend_filterCategory_filterTopics.fillna(0)
        
        
        total_trend_filterCategory_filterTopics__value = total_trend_filterCategory_filterTopics['Value'].values.tolist()
        
        total_filterCategory_alltopics_data_count = np.array(total_trend_filterCategory_allTopics__value)
        total_filterCategory_filtertopics_data_count = np.array(total_trend_filterCategory_filterTopics__value)
        new_totaly = list(total_filterCategory_alltopics_data_count - total_filterCategory_filtertopics_data_count)
        
        
        # telegram_association_filterCategory_filterTopic = result_telegram_filterCategory_filterTopic['associations']
        # assoc_fig_labels = []
        # assoc_fig_parent = []
        # assoc_fig_value = []
        # assoc_fig_labels.append('Association')
        # assoc_fig_parent.append('')
        # assoc_fig_value.append(100)
        
        
        # for ind,item in enumerate(list(telegram_association_filterCategory_filterTopic.keys())):
        #     fraction = round(100/len(telegram_association_filterCategory_filterTopic),3)
        #     if((ind + 1) < len(list(telegram_association_filterCategory_filterTopic.keys()))):
        #         assoc_fig_labels.append(item)
        #         assoc_fig_parent.append('Association')
        #         assoc_fig_value.append(fraction)
        #         tempData = telegram_association_filterCategory_filterTopic[item]
        #         totalSum = sum([pair[1] for pair in tempData])
        #         temp_sum = 0
        #         for ind2,pair in enumerate(tempData):
        #             fraction2 = round((pair[1]*fraction)/totalSum,3)
        #             if((ind2 +1 ) < len(tempData)):
        #                 temp_sum += fraction2
        #                 assoc_fig_labels.append(pair[0])
        #                 assoc_fig_parent.append(item)
        #                 assoc_fig_value.append(fraction2)
        #             else:
        #                 assoc_fig_labels.append(pair[0])
        #                 assoc_fig_parent.append(item)
        #                 assoc_fig_value.append(round(fraction - round(temp_sum,3),3)-0.001)
        #     else:
        #         fraction_final = round((100 - (fraction*(len(list(telegram_association_filterCategory_filterTopic.keys())) - 1))),3)
        #         assoc_fig_labels.append(item)
        #         assoc_fig_parent.append('Association')
        #         assoc_fig_value.append(fraction_final)
        #         tempData = telegram_association_filterCategory_filterTopic[item]
        #         totalSum = sum([pair[1] for pair in tempData])
        #         temp_sum = 0
        #         for ind2,pair in enumerate(tempData):
        #             fraction3 = round((pair[1]*fraction_final)/totalSum,3)
        #             if((ind2+1) < len(tempData)):
        #                 temp_sum += fraction3
        #                 assoc_fig_labels.append(pair[0])
        #                 assoc_fig_parent.append(item)
        #                 assoc_fig_value.append(fraction3)
        #             else:
        #                 assoc_fig_labels.append(pair[0])
        #                 assoc_fig_parent.append(item)
        #                 assoc_fig_value.append(round(fraction_final - round(temp_sum,3),3)-0.001)
        #
        # fig_association_telegram =go.Figure(go.Sunburst(
        #         labels=assoc_fig_labels,
        #         parents=assoc_fig_parent,
        #         values=assoc_fig_value,
        #         branchvalues="total",
        #         ))
        # fig_association_telegram.update_layout(margin = dict(t=20, l=0, r=0, b=40), height=700)

        
        influencer_list = result_telegram_filterCategory_filterTopic['influencers']
        influencerDivChild = []
        maxInfCount = influencer_list[0][1][0]
        minInfCount = influencer_list[-1][1][0]
        tot_inf_telegram_count = 0
        
        dataframe_temp = []
        for i,inf in enumerate(influencer_list):
            tot_inf_telegram_count += inf[1][0]
            changeScale = (((inf[1][0] - minInfCount) * 60)/(maxInfCount - minInfCount)) + 40
            barWidth = str(changeScale) + '%'
            dataframe_temp.append([inf[0], inf[1][2], inf[1][1],inf[1][0]])
            inf_layout = html.Div([
                    html.Div([
                            html.Div([
                                    html.P(str(inf[0]),style={'float':'left', 'marginLeft':'2%', 'fontSize':14,'marginTop':5}),
                                    html.P(str(inf[1][0]),style={'float':'right', 'marginRight':'2%', 'marginTop':5,'fontSize':14})
                                    ],style={'width':barWidth, 'background':'#6adddd', 'height':30, 'minHeight':1})
                            ], className='InfluencerBarChartDivStyle'),
                    ],className='FullWidth FloatLeft MarginTop20Px')
            influencerDivChild.append(inf_layout)
        
        avg_visit_per_post_df = pd.DataFrame(data=dataframe_temp, columns=['Name','Value','Visit','Count'])
#        avg_visit_per_post_df = avg_visit_per_post_df.sort_values(ascending=False, by=['Value'])
        
        
        post_count_df = avg_visit_per_post_df.sort_values(ascending=False, by=['Count'])
        post_visit_df = avg_visit_per_post_df.sort_values(by=['Visit'])
        
        
        channelNameList = []
        cumulativeVisitCount = []
        cumulativeVisitCount_temp = 0
        firstQuartileValue = int(sum(total_trend_filterCategory_filterTopics__value)/4)
        secondQuartileValue = int(sum(total_trend_filterCategory_filterTopics__value)/2)
        thirdQuartileValue = int(sum(total_trend_filterCategory_filterTopics__value)*3/4)
        firstQuartileFlag = 0
        secondQuartileFlag = 0
        thirdQuartileFlag = 0
        
        firstQuartileIndex = -1 
        secondQuartileIndex = -1
        thirdQuartileIndex = -1
        
        
        for i, value in enumerate(post_count_df['Count'].values.tolist()):
            cumulativeVisitCount_temp += value
            channelNameList.append(post_count_df.iloc[i]['Name'])
            cumulativeVisitCount.append(cumulativeVisitCount_temp)
            if(cumulativeVisitCount_temp >= firstQuartileValue and firstQuartileFlag == 0):
                firstQuartileIndex = i
                firstQuartileFlag = 1
            if(cumulativeVisitCount_temp >= secondQuartileValue and secondQuartileFlag == 0):
                secondQuartileIndex = i
                secondQuartileFlag = 1
            if(cumulativeVisitCount_temp >= thirdQuartileValue and thirdQuartileFlag == 0):
                thirdQuartileIndex = i
                thirdQuartileFlag = 1
        channelNameList.append('Other')
        cumulativeVisitCount.append(int(sum(total_trend_filterCategory_filterTopics__value)))
        
        firstQString = str(firstQuartileIndex + 1) + '  '
        secondQString = str(secondQuartileIndex + 1) + '  '
        thirdQString = str(thirdQuartileIndex + 1) + '  '
        
        if(firstQuartileIndex == -1):########
            firstQString = '> 100  '
        if(secondQuartileIndex == -1):########
            secondQString = '> 100  '
        if(thirdQuartileIndex == -1):########
            thirdQString = '> 100  '
        
        
        figSubplot = make_subplots(rows=1, cols=2,shared_xaxes=False,
                    shared_yaxes=True, vertical_spacing=0.001)

        nameList = post_visit_df['Name'].values.tolist()[-15:]
        visitList = post_visit_df['Visit'].values.tolist()[-15:]
        avgVisitList = post_visit_df['Value'].values.tolist()[-15:]
        figSubplot.append_trace(go.Bar(
            x=visitList,
            y=nameList,
            marker=dict(
                color='rgba(60,124,181, 1)',
                line=dict(
                    color='rgba(60,124,181, 1)',
                    width=1),
            ),
            orientation='h',
        ), 1, 1)
        
        figSubplot.append_trace(go.Scatter(
            x=avgVisitList, 
            y=nameList,
            mode='lines+markers',
            line_color='rgb(76,153,160)',
            marker_size=10
        ), 1, 2)

        figSubplot.update_layout(
            title='Channels Total posts views and Average view per post (Top 15)',
            height=900,
            yaxis=dict(
                showgrid=True,
                showline=False,
                gridcolor='#eee',
                showticklabels=True,
                domain=[0, 1]
            ),
            yaxis2=dict(
                showgrid=True,
                showline=True,
                gridcolor = '#eee',
                showticklabels=False,
                linecolor='rgba(102, 102, 102, 0.5)',
                linewidth=2,
                domain=[0, 1],
            ),
            xaxis=dict(
                zeroline=False,
                showline=False,
                showticklabels=True,
                showgrid=True,
                domain=[0, 0.5],
                title=dict(text='Total Channels Post Views')
            ),
            xaxis2=dict(
                zeroline=False,
                showline=False,
                showticklabels=True,
                showgrid=True,
                domain=[0.5, 1],
                side='top',
                dtick=200000,
                title=dict(text='Channels Average View per Post')
            ),
            showlegend=False,
            margin=dict(l=100, r=20, t=120, b=70),
            paper_bgcolor='rgb(255, 255, 255)',
            plot_bgcolor='rgb(255, 255, 255)',
        )
        
        
        inf_subplot = [dcc.Graph(
                    id=str(time.time()),
                    figure=figSubplot
                )]
        
        dictPostCumuCount = {
                    'data': [
                            go.Bar(
                                    x=channelNameList,
                                    y=cumulativeVisitCount,
                                    marker=go.bar.Marker(
                                            color='rgb(60,124,181)',
                                            )
                                    ),
                            go.Scatter(x=[channelNameList[0],channelNameList[thirdQuartileIndex]], 
                                       y=[thirdQuartileValue,thirdQuartileValue], 
                                       mode='lines+text',
                                       line=dict(width=2, color='#D1CE42'), 
                                       fill='tozeroy',
                                       fillcolor='rgba(209, 206, 66, 0.4)',
                                       text=['Third Quartile',''], 
                                       textposition='top right'),
                            go.Scatter(x=[channelNameList[0],channelNameList[secondQuartileIndex]], 
                                       y=[secondQuartileValue,secondQuartileValue], 
                                       mode='lines+text',
                                       line=dict(width=2, color='#45D3D3'), 
                                       fill='tozeroy',
                                       fillcolor='rgba(69, 211, 211, 0.2)',
                                       text=['Second Quartile',''], 
                                       textposition='top right'),
                            go.Scatter(x=[channelNameList[0],channelNameList[firstQuartileIndex]], 
                                       y=[firstQuartileValue,firstQuartileValue], 
                                       mode='lines+text',
                                       line=dict(width=2, color='#E92910'),
                                       fill='tozeroy',
                                       fillcolor='rgba(233, 41, 16, 0.3)', 
                                       text=['First Quartile',''], 
                                       textposition='top right')
                            ],
                            'layout': go.Layout(
                                    showlegend=False,
                                    title=dict(text='Telegram Channels Post Cumulative Count Graph'),
                                    height=600,
                                    margin=dict(b=200 ,l=80),
                                    annotations=[
                                        go.layout.Annotation(
                                            x=0.5,
                                            y=-0.55,
                                            showarrow=False,
                                            text="Channels",
                                            font=dict(size=18),
                                            xref="paper",
                                            yref="paper"
                                        )],
                                    yaxis=dict(title=dict(text='Cumulative Posts Count')),
                                    )                
                }
        # eventLineMaxValue = max(total_trend_filterCategory_allTopics__value)+50
        # if(eventValue != None):
        #     dictTotalTrend = {
        #             'data': [
        #                     go.Bar(
        #                             x=total_trend_filterCategory_allTopics__time,
        #                             y=total_trend_filterCategory_filterTopics__value,
        #                             name='Selected Topic',
        #                             marker=go.bar.Marker(
        #                                     color='rgb(60,124,181)',
        #                                     )
        #                             ),
        #                     go.Bar(
        #                             x=total_trend_filterCategory_allTopics__time,
        #                             y=new_totaly,
        #                             name='total',
        #                             marker=go.bar.Marker(
        #                                     color='rgb(205,231,241)',
        #                                     )
        #                             ),
        #                     go.Scatter(x=[eventValue,eventValue], y=[0,eventLineMaxValue], mode='lines',line=dict(width=2, color='red'))
        #                     ],
        #                     'layout': go.Layout(
        #                             showlegend=True,
        #                             barmode='stack',
        #                             xaxis=dict(title=dict(text='Time')),
        #                             yaxis=dict(title=dict(text='Channels Post Count')),
        #                             )
        #             }
        if True:
            dictTotalTrend = {
                    'data': [
                            go.Bar(
                                    x=total_trend_filterCategory_allTopics__time,
                                    y=total_trend_filterCategory_filterTopics__value,
                                    name='Selected Topic',
                                    marker=go.bar.Marker(
                                            color='rgb(60,124,181)',
                                            )
                                    ),
                            go.Bar(
                                    x=total_trend_filterCategory_allTopics__time,
                                    y=new_totaly,
                                    name='total',
                                    marker=go.bar.Marker(
                                            color='rgb(205,231,241)',
                                            )
                                    )
                            ],
                            'layout': go.Layout(
                                    showlegend=True,
                                    barmode='stack',
                                    xaxis=dict(title=dict(text='Time')),
                                    yaxis=dict(title=dict(text='Channels Post Count')),
                                    )
                    }

        x = result_telegram_filterCategory_filterTopic['topic_aggregation']
        sorted_x = sorted(x.items(), key=operator.itemgetter(1))
        data_label = []
        data_value = []
        for item in sorted_x:
            if(item[1] > 0.05):
                data_label.append(item[0])
                data_value.append(item[1])
        fig = SquarifyChart(200,100,data_value,data_label,1400,700,9,'Telegram Topic Frequency')
        
        
        return dictTotalTrend,influencerDivChild,format(int(sum(total_trend_filterCategory_filterTopics__value)),',d'),format(tot_inf_telegram_count,',d'),dictPostCumuCount,firstQString,secondQString,thirdQString,inf_subplot, fig, options
    else:
        return {},[],'','',{},'','','',[], {}, options
