from vision.data_exploration import DataExploration
import numpy as np
import pandas as pd

def visualize(data_model: DataExploration):
    result = data_model.data_statistics('twitter', [], [])
    return result


if __name__ == '__main__':
    data_model = DataExploration()
    data = visualize(data_model)
    totalTrendDataframeData = []
    for item in data['trend']:
        totalTrendDataframeData.append([item[0]])
        
    totalTrend = pd.DataFrame(data=totalTrendDataframeData, columns=['Time'])
    
    totalSnappDataframeData = []
    
    snapData = data_model.data_statistics('twitter', ['Karpino'], [])
    
    for item in snapData['trend']:
        totalSnappDataframeData.append([item[0],item[1][0]])
    
    snapTrend = pd.DataFrame(data=totalSnappDataframeData, columns=['Time','Value'])
    
    finalTrend = totalTrend.merge(snapTrend, how='outer', on=['Time'])
    finalTrend = finalTrend.fillna(0)
    
