import json
import pandas as pd


def read_trend(trend_file_path):    
    dataTemp = []
    with open(trend_file_path, 'r') as file:
        for line in file:
            data = json.loads(line)
            dataTemp.append([data['category'],data['date'],data['domain'],data['topics']])

    return dataTemp


data = read_trend('data/news_trend.json')


df = pd.DataFrame(data=data, columns=['Category','Date','Domain','Topics'])
df_snapp = df[df.Category.map(lambda x: 'Snapp' in x)]

topics = df_snapp['Topics']

topicList = []
topics.map(lambda x: topicList.append(list(x.keys())))

topicList = [item for sublist in topicList for item in sublist]
topicList = list(set(topicList))

topicScoreDict = {}
for key in topicList:
    topicScoreDict[key] = 0
    
for i in range(0,len(df_snapp)):
    rowTopicsList = list(df_snapp.iloc[i]['Topics'].keys())
    for key in rowTopicsList:
        topicScoreDict[key] = topicScoreDict[key] + df_snapp.iloc[i]['Topics'][key]
