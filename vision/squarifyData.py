# -*- coding: utf-8 -*-

import plotly.graph_objs as go

import squarify


def createColorScalePallet(minR, minG, minB, maxR, maxG, maxB, midPoint):
    rSeq = int((minR - maxR)/(midPoint+1))
    gSeq = int((minG - maxG)/(midPoint+1))
    bSeq = int((minB - maxB)/(midPoint+1))
    
    colorPallet = []
    colorStr = 'rgb('+str(minR)+','+str(minG)+','+str(minB)+')'
    colorPallet.append(colorStr)
    for i in range(1, (midPoint+1)):
        tmpR = minR - (rSeq*i)
        tmpG = minG - (gSeq*i)
        tmpB = minB - (bSeq*i)
        colorStr = 'rgb('+str(tmpR)+','+str(tmpG)+','+str(tmpB)+')'
        colorPallet.append(colorStr)
    colorStr = 'rgb('+str(maxR)+','+str(maxG)+','+str(maxB)+')'
    colorPallet.append(colorStr)
    return colorPallet

def SquarifyChart(width, height, values, labels,chartWidth, chartHeight, topicCount, chartName):
    fig = go.Figure()
    
    x = 0.
    y = 0.
    width = width
    height = height
    
    
    normed = squarify.normalize_sizes(values, width, height)
    rects = squarify.squarify(normed, x, y, width, height)
    
    color_brewer = createColorScalePallet(234,252,253, 72,134,187,topicCount)
    
    shapes = []
    annotations = []
    
    for r, label, val, color in zip(rects, labels, values, color_brewer):
        shapes.append(
            dict(
                type = 'rect',
                x0 = r['x'],
                y0 = r['y'],
                x1 = r['x']+r['dx'],
                y1 = r['y']+r['dy'],
                line = dict( width = 3, color='#666'),
                fillcolor = color
            )
        )
        annotations.append(
            dict(
                x = r['x']+(r['dx']/2),
                y = r['y']+(r['dy']/2),
                text = label,
                font=dict(
                        color="black",
                        size=18
                ),
                showarrow = False
            )
        )
    
    fig.add_trace(go.Scatter(
        x = [ r['x']+(r['dx']/2) for r in rects ],
        y = [ r['y']+(r['dy']/2) for r in rects ],
        text = [ str(v) for v in labels ],
        mode = 'text',
    ))
    
    fig.layout = dict(
        margin=dict(l=0, r=0, t=50, b=20),
        title=go.layout.Title(text=chartName,xanchor='center',yanchor= 'top',x=0.5),
        xaxis=dict(showgrid=False,zeroline=False,showticklabels=False),
        yaxis=dict(showgrid=False,zeroline=False,showticklabels=False),
        shapes=shapes,
        annotations=annotations,
        hovermode='closest'
    )
    
    
    return fig