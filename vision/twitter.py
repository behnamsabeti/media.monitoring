# -*- coding: utf-8 -*-

import plotly.graph_objs as go
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import numpy as np
from vision.squarifyData import SquarifyChart
from vision.app import app
from plotly.subplots import make_subplots
from vision.data_exploration import DataExploration
import operator
import base64
from dash.exceptions import PreventUpdate
from datetime import datetime, timedelta


userAvatar_fileName = 'vision/images/users.png'

encoded_image_avatar = base64.b64encode(open(userAvatar_fileName, 'rb').read())

def createSubplotFigure(emotionData, rowCount, colCount, eventValue=None):
    xAxis = emotionData['Time'].values.tolist()
    emotions = list(emotionData.keys())
    emotions.remove('Time')
    fig = make_subplots(
    rows=rowCount, cols=colCount,
    shared_xaxes=True,
    shared_yaxes=False,
    vertical_spacing=0.05,
    horizontal_spacing=0.08,
    subplot_titles=tuple(emotions)
    )
    for i in range(0,rowCount):
        maxValue = []
        for j in range(0,colCount):
            
            temp= emotions[(i*2)+j]
            maxValue = max(emotionData[temp].values.tolist())
            minValue = min(emotionData[temp].values.tolist())
                
            dataFrameKey = emotions[(i*2)+j]
            fig.add_trace(
                    go.Scatter(
                            x=xAxis,
                            y=emotionData[dataFrameKey].values.tolist(),
                            name=dataFrameKey,
                            mode='lines',
                            line=dict(color='#378fff')
                            ),
                            row=(i+1), col=(j+1)
                        )
            if(eventValue != None):
                fig.add_trace(
                    go.Scatter(x=[eventValue,eventValue], y=[minValue,maxValue], mode='lines',line=dict(width=2, color='red')),
                    row=(i+1), col=(j+1)
                    )

                
                        
    fig.update_layout(
        height=600,
        showlegend=False,
        title=go.layout.Title(text='Emotion',xanchor='center',yanchor= 'top',x=0.5),
        margin={'l': 40, 'b': 40, 't': 80, 'r': 40},
        )
    return fig


def prepareBarPolardata(emotionData):
    cols = ['emotions', 'strength', 'frequency']
    dataValues = emotionData['Value']
    data_max = (int(max(dataValues)*10)+1)/10
    strength_step = data_max/10.0
    strength = []
    steps = list(np.arange(0,data_max,strength_step))
    for i in steps:
        strength.append(str(i)+'-'+str(i+strength_step))

    
    dataSet = []
    for i in range(0,len(emotionData)):
        power = emotionData.iloc[i]['Value']
        integer = int(power/strength_step)
        fraction = (power/strength_step) - integer
        for j in range(0,integer):
            dataSet.append([emotionData.iloc[i]['Emotion'], strength[j],1])
        if(fraction != 0):
            dataSet.append([emotionData.iloc[i]['Emotion'], strength[integer],fraction])
    finalData = pd.DataFrame(np.array(dataSet), columns=cols)
    return finalData
    



# start_time = '2019-06-01'
# end_time = '2019-10-03'
    
data_model = DataExploration()


    
def CreateTopicList(topicList):
    topics = []
    for topic in topicList:
        topics.append({'label':topic, 'value':topic})
    return topics




totalSnappDataframeData = []
# result_twitter_snapp = data_model.data_statistics('twitter', [])

# topicsList = list(result_twitter['topic_aggregation'].keys())
topicOptionList = CreateTopicList([])


# x = result_twitter_snapp['topic_aggregation']
# sorted_x = sorted(x.items(), key=operator.itemgetter(1))
# data_label = []
# data_value = []
# for item in sorted_x:
#     if(item[1] != 0):
#         data_label.append(item[0])
#         data_value.append(item[1])


layout = html.Div([
    html.H1('Twitter'),
    html.Div([
        html.P('Query Name'),
        dcc.Dropdown(
            id='twitter-query-dropdown',
            options=[{'label': l, 'value': l} for l in data_model.query_names()
            ],
            value=None,
                    )
            ],className='Width50Percent FloatLeft'),
    # dcc.Graph(
    #     id='topic-graph',
    #     figure={},
    #     config={
    #             'displayModeBar': False
    #         },
    #     className='FullWidth FloatLeft'
    # ),


    # html.Div([
    #         html.Button('Submit', id='submit-button', className='ButtonStyle')
    #         ], className='Width50Percent FloatLeft'),
    # html.Div([
    #     dcc.Dropdown(
    #         id='twitter-event-dropdown',
    #         options=[
    #                 {'label': 'Snapp Hijab Crisis', 'value':'2019-06-06'}
    #                 ],
    #         placeholder='Choose an event'
    #                 )
    #         ],className='Width50Percent FloatLeft MarginRight5Percent MarginTop40Px'),
    html.Div([
            # html.Div(id='event-hidden-div',children=[
            #         html.Div([html.H1('Event Comparision Segment', className='VisualSegmentTitle')],className='FullWidth FloatLeft'),
            #         html.Div([
            #                 dcc.Graph(
            #                         id='twitter-event-sentiment-compare',
            #                         figure={}
            #                         )
            #                 ],className='FullWidth FloatLeft'),
            #         html.Div([
            #                 dcc.Graph(
            #                         id='twitter-event-emotion-compare',
            #                         figure={}
            #                         )
            #                 ],className='FullWidth FloatLeft'),
            #         ],style={'display':'none'},className='EventHiddenRowDivStyle'),
            html.Div([
                    dcc.Graph(
                            id='total-trend',
                            figure={}
                            )
                ],className='RowDivStyle'),
            html.Div([
                    html.Div([
                            dcc.Graph(
                                    id='sentiment-pieChart',
                                    figure={},
                                    config={
                                    'displayModeBar': False
                                    }
                                            )
                            ],className='HalfRowLeftDivStyle'),
                    html.Div([
                            dcc.Graph(
                                    id='sentiment-trend-total',
                                    figure={},
                                    config={
                                        'displayModeBar': False
                                        }   
                                        ),
                            dcc.Graph(
                                    id='sentiment-trend-normal',
                                    figure={},
                                    config={
                                        'displayModeBar': False
                                        }
                                        ),
                            ],className='HalfRowRightDivStyle')
            ],className='RowDivStyle'),
            html.Div([
                    html.Div([
                            dcc.Graph(
                                    id='emotion-bar-polar',
                                    figure={},
                                    config={
                                            'displayModeBar': False
                                            }
                                            )
                            ],className='HalfRowLeftDivStyle'),
                    html.Div([
                            dcc.Graph(
                                    id='emotion-trend-total',
                                    figure={},
#                                    config={
#                                            'displayModeBar': False
#                                            }
                                        )
                            ],className='HalfRowRightDivStyle')
            ],className='RowDivStyle'),
            html.Div([
                    html.Div(id='influencer-segment',children=[
                            ], className='InfluencerListSegmentDivStyle'),
                    html.Div([
                            html.Div([
                                    html.P('Total Tweet', className='TotalTwitteTextLabel'),
                                    html.P(id='totalTwittCount',children='', className='TotalTwitteTextValue'),
                                    ], className='InfluencerDataTotalDivStyle'),
                            html.Div([
                                    html.P('Total Influencer Tweet', className='TotalInfluencerTwitteTextLabel'),
                                    html.P(id='totalInfluencerTwittCount',children='', className='TotalInfluencerTwitteTextValue'),
                                    ], className='InfluencerDataTotalInfluencerDivStyle'),
                            # html.Div([
                            #         html.P('Bot Influencer percentage', className='TotalInfluencerTwitteTextLabel'),
                            #         html.P(id='influencerBotPercentage',children='', className='TotalInfluencerTwitteTextValue'),
                            #         ], className='InfluencerDataBotPercentDivStyle')
                            ], className='InfluencerDataAnalysisDivStyle'),
            ], className='RowDivStyle')
            
    ],className='TwitterVisBodyStyle'),
    
    ], className='LandingBodyDivStyle')

# @app.callback(dash.dependencies.Output('twitter-dropdown-div', 'children'),
#               [dash.dependencies.Input('twitter-dropdown', 'value')],
#               [dash.dependencies.State('twitter-dropdown-div', 'children')])
# def topicDropDownUpdate(valueTopic,prevDivChild):
#     if(prevDivChild[0]['props']['value'] == valueTopic):
#         raise PreventUpdate
#     if(valueTopic == []):
#         valueTopic = data_label
#     dropdown = dcc.Dropdown(
#         id='twitter-dropdown',
#         options=topicOptionList,
#         value=valueTopic,
#         multi=True,
#         clearable=False
#         )
#     return dropdown

        
    
                                 
# @app.callback(dash.dependencies.Output('topic-graph', 'figure'),
#               [dash.dependencies.Input('twitter-category-dropdown', 'value')])
# def topicChartUpdate(valueCategory):
#     result_twitter_category = data_model.data_statistics('twitter', valueCategory, [],start_time, end_time)
#     x = result_twitter_category['topic_aggregation']
#     sorted_x = sorted(x.items(), key=operator.itemgetter(1))
#     data_label = []
#     data_value = []
#     for item in sorted_x:
#         if(item[1] != 0):
#             data_label.append(item[0])
#             data_value.append(item[1])
#     fig = SquarifyChart(200,100,data_value,data_label,1400,700,5,'Twitter Topic Frequency')
#     return fig

@app.callback([dash.dependencies.Output('total-trend', 'figure'),
               dash.dependencies.Output('sentiment-pieChart', 'figure'),
               dash.dependencies.Output('sentiment-trend-total', 'figure'),
               dash.dependencies.Output('sentiment-trend-normal', 'figure'),
               dash.dependencies.Output('emotion-bar-polar', 'figure'),
               dash.dependencies.Output('emotion-trend-total', 'figure'),
               dash.dependencies.Output('influencer-segment', 'children'),
               dash.dependencies.Output('totalTwittCount', 'children'),
               dash.dependencies.Output('totalInfluencerTwittCount', 'children'),
               # dash.dependencies.Output('influencerBotPercentage', 'children'),
               dash.dependencies.Output('twitter-query-dropdown', 'options'),
              #  dash.dependencies.Output('event-hidden-div', 'style'),
              #  dash.dependencies.Output('twitter-event-sentiment-compare', 'figure'),
              #  dash.dependencies.Output('twitter-event-emotion-compare', 'figure')],
              dash.dependencies.Input('twitter-query-dropdown', 'value')
              #  dash.dependencies.Input('twitter-category-dropdown', 'value'),
              #  dash.dependencies.Input('twitter-event-dropdown', 'value')],
               ])
def trendChartUpdate(value):
    options = [{'label': l, 'value': l} for l in data_model.query_names()]
    if value is not None:
        # value = value[0]
        print(value)
        result_twitter = data_model.data_statistics(value, 'twitter', [])
        totalTrendDataframeData_temp = []
        for item in result_twitter['trend']:
            totalTrendDataframeData_temp.append([item[0]])
        totalTrend = pd.DataFrame(data=totalTrendDataframeData_temp, columns=['Time'])
        trend_filterCategory_allTopics_dataframe_temp_temp = []
        result_twitter_filterCategory_allTopic_temp = data_model.data_statistics(value, 'twitter', [])
        for item in result_twitter_filterCategory_allTopic_temp['trend']:
            trend_filterCategory_allTopics_dataframe_temp_temp.append([item[0],item[1][0]])
        trend_filterCategory_allTopics_dataframe_temp_ = pd.DataFrame(data=trend_filterCategory_allTopics_dataframe_temp_temp, columns=['Time','Value'])
        total_trend_filterCategory_allTopics_ = totalTrend.merge(trend_filterCategory_allTopics_dataframe_temp_, how='outer', on=['Time'])
        total_trend_filterCategory_allTopics_ = total_trend_filterCategory_allTopics_.fillna(0)
        
        
        total_trend_filterCategory_allTopics__time = total_trend_filterCategory_allTopics_['Time'].values.tolist()
        total_trend_filterCategory_allTopics__value = total_trend_filterCategory_allTopics_['Value'].values.tolist()
        
        trend_filterCategory_filterTopics_dataframe_temp = []
        result_twitter_filterCategory_filterTopic = data_model.data_statistics(value, 'twitter', [])
        for item in result_twitter_filterCategory_filterTopic['trend']:
            trend_filterCategory_filterTopics_dataframe_temp.append([item[0],item[1][0]])
        trend_filterCategory_filterTopics_dataframe = pd.DataFrame(data=trend_filterCategory_filterTopics_dataframe_temp, columns=['Time','Value'])
        total_trend_filterCategory_filterTopics = totalTrend.merge(trend_filterCategory_filterTopics_dataframe, how='outer', on=['Time'])
        total_trend_filterCategory_filterTopics = total_trend_filterCategory_filterTopics.fillna(0)
        
        
        total_trend_filterCategory_filterTopics__value = total_trend_filterCategory_filterTopics['Value'].values.tolist()
        
        total_filterCategory_alltopics_data_count = np.array(total_trend_filterCategory_allTopics__value)
        total_filterCategory_filtertopics_data_count = np.array(total_trend_filterCategory_filterTopics__value)
        new_totaly = list(total_filterCategory_alltopics_data_count - total_filterCategory_filtertopics_data_count)
        
        
        average_sentiment_filterCategory_filterTopics = result_twitter_filterCategory_filterTopic['average_sentiment']
        average_sentiment_filterCategory_filterTopics_label = list(average_sentiment_filterCategory_filterTopics.keys())
        average_sentiment_filterCategory_filterTopics_value = list(average_sentiment_filterCategory_filterTopics.values())
        average_sentiment_filterCategory_filterTopics_color = []
        for key in average_sentiment_filterCategory_filterTopics_label:
            if key == 'Negative':
                average_sentiment_filterCategory_filterTopics_color.append('#d84132')
            elif key == 'Positive':
                average_sentiment_filterCategory_filterTopics_color.append('#35b14a')
            else:
                average_sentiment_filterCategory_filterTopics_color.append('#378fff')
        
        trend_sentiment_filterCategory_filterTopics = result_twitter_filterCategory_filterTopic['sentiment_trend']
        trend_sentiment_filterCategory_filterTopics_dataframe_temp = []
        for item in trend_sentiment_filterCategory_filterTopics:
            trend_sentiment_filterCategory_filterTopics_dataframe_temp.append([item[0],item[1]['Negative'],item[1]['Positive'],item[1]['Neutral']])
        trend_sentiment_filterCategory_filterTopics_dataframe = pd.DataFrame(data=trend_sentiment_filterCategory_filterTopics_dataframe_temp, columns=['Time','Negative','Positive','Neutral'])
        total_trend_sentiment_filterCategory_filterTopics = totalTrend.merge(trend_sentiment_filterCategory_filterTopics_dataframe, how='outer', on=['Time'])
        total_trend_sentiment_filterCategory_filterTopics = total_trend_sentiment_filterCategory_filterTopics.fillna(0)
        
        negative_trend_filterCategory_filterTopics = []
        positive_trend_filterCategory_filterTopics = []
        neutral_trend_filterCategory_filterTopics = []
        
        negative_trend_normal_filterCategory_filterTopics = []
        positive_trend_normal_filterCategory_filterTopics = []
        neutral_trend_normal_filterCategory_filterTopics = []
        for i in range(0,len(total_trend_sentiment_filterCategory_filterTopics)):
            negative_trend_filterCategory_filterTopics.append(total_trend_sentiment_filterCategory_filterTopics.iloc[i]['Negative']*total_trend_filterCategory_filterTopics__value[i])
            positive_trend_filterCategory_filterTopics.append(total_trend_sentiment_filterCategory_filterTopics.iloc[i]['Positive']*total_trend_filterCategory_filterTopics__value[i])
            neutral_trend_filterCategory_filterTopics.append(total_trend_sentiment_filterCategory_filterTopics.iloc[i]['Neutral']*total_trend_filterCategory_filterTopics__value[i])
            
            negative_trend_normal_filterCategory_filterTopics.append(total_trend_sentiment_filterCategory_filterTopics.iloc[i]['Negative'])
            positive_trend_normal_filterCategory_filterTopics.append(total_trend_sentiment_filterCategory_filterTopics.iloc[i]['Positive'])
            neutral_trend_normal_filterCategory_filterTopics.append(total_trend_sentiment_filterCategory_filterTopics.iloc[i]['Neutral'])
            
        average_emotion_filterCategory_filterTopics = result_twitter_filterCategory_filterTopic['average_emotion']
        emotion_average_emotion = list(average_emotion_filterCategory_filterTopics.keys())
        emotion_average_value = list(average_emotion_filterCategory_filterTopics.values())
        
        happy_index = emotion_average_emotion.index('Happy')
        del emotion_average_emotion[happy_index]
        del emotion_average_value[happy_index]
        emotionData = {
        'Emotion':emotion_average_emotion,
        'Value':emotion_average_value
        }
        emotionDataframe = pd.DataFrame(data=emotionData)
        df = prepareBarPolardata(emotionDataframe)
        figEmotion = px.bar_polar(df, r="frequency", theta="emotions", color="strength",color_discrete_sequence= px.colors.sequential.deep[:-3])
        figEmotion.update_layout(
                showlegend=False,
                title=go.layout.Title(text='Emotion',xanchor='center',yanchor= 'top',x=0.5),
                margin={'l': 40, 'b': 40, 't': 80, 'r': 40},
                )
        trend_emotion_filterCategory_filterTopics = result_twitter_filterCategory_filterTopic['emotion_trend']
        trend_emotion_filterCategory_filterTopics_dataframe_temp = []
        for item in trend_emotion_filterCategory_filterTopics:
            trend_emotion_filterCategory_filterTopics_dataframe_temp.append([item[0],item[1]['Happy'],item[1]['Sad'],item[1]['Angry'],item[1]['Calm'],item[1]['Love'],item[1]['Amazed'],item[1]['Excited'],item[1]['Afraid'],item[1]['Frustrated'],item[1]['Emotionless']])
        trend_emotion_filterCategory_filterTopics_dataframe = pd.DataFrame(data=trend_emotion_filterCategory_filterTopics_dataframe_temp, columns=['Time','Happy','Sad','Angry','Calm','Love','Amazed','Excited','Afraid','Frustrated','Emotionless'])
        total_trend_emotion_filterCategory_filterTopics = totalTrend.merge(trend_emotion_filterCategory_filterTopics_dataframe, how='outer', on=['Time'])
        total_trend_emotion_filterCategory_filterTopics = total_trend_emotion_filterCategory_filterTopics.fillna(0)
        
      
        influencer_list = result_twitter_filterCategory_filterTopic['influencers']
        influencerDivChild = []
        maxInfCount = influencer_list[0][1][0]
        minInfCount = influencer_list[-1][1][0]
        tot_inf_twitt_count = 0
        for i,inf in enumerate(influencer_list):
            tot_inf_twitt_count += inf[1][0]
            
            changeScale = (((inf[1][0] - minInfCount) * 60)/(maxInfCount - minInfCount)) + 40
            barWidth = str(changeScale) + '%'
            
            #barWidth = str(int((inf[1][0]/maxInfCount)*100)) + '%'
            inf_layout = html.Div([
                    html.Div([
                            html.Div([
                                    html.A([
                                            html.P(str(inf[0]),style={'float':'left', 'marginLeft':'2%', 'fontSize':14,'marginTop':5})
                                            ],href=inf[1][1]),
                                    html.P(str(inf[1][0]),style={'float':'right', 'marginRight':'2%', 'marginTop':5,'fontSize':14})
                                    ],style={'width':barWidth, 'background':'#6adddd', 'height':30, 'minHeight':1})
                            ], className='InfluencerBarChartDivStyle'),
                    ],className='FullWidth FloatLeft MarginTop20Px')
            influencerDivChild.append(inf_layout)
        
        
        # eventLineMaxValue = max(total_trend_filterCategory_allTopics__value)+50
        # if(eventValue != None):
        #     eventTime = datetime.strptime(eventValue, "%Y-%m-%d")
        #     startTime = eventTime - timedelta(days=5)
        #     endTime = eventTime + timedelta(days=5)
        #
        #     event_data_result = data_model.event_statistics(eventValue,str(startTime)[:10], str(endTime)[:10])
        #
        #     sentimentAfter = event_data_result['sentiment']['after']
        #     sentimentBefore = event_data_result['sentiment']['before']
        #
        #     figSentimentComparision = go.Figure(data=[
        #             go.Bar(name='Before', x=['Positive', 'Neutral', 'Negative'], y=[sentimentBefore['positive'],sentimentBefore['neutral'],sentimentBefore['negative']],marker_color='rgb(76,153,160)'),
        #             go.Bar(name='After', x=['Positive', 'Neutral', 'Negative'], y=[sentimentAfter['positive'],sentimentAfter['neutral'],sentimentAfter['negative']],marker_color='rgb(62,82,143)')
        #             ])
        #     figSentimentComparision.update_layout(barmode='group',
        #                                           height=400,
        #                                           showlegend=False,
        #                                           margin={'l': 30, 'b': 20, 't': 30, 'r': 20},
        #                                           title=dict(text='Sentiment Comparision'),
        #                                           yaxis=dict(showgrid=True,showline=True,gridcolor='#eee'),
        #                                           xaxis=dict(showgrid=False,showline=True),
        #                                           plot_bgcolor='rgb(255, 255, 255)')
        #
        #
        #     emotionAfter = event_data_result['emotion']['after']
        #     emotionBefore = event_data_result['emotion']['before']
        #
        #     emotion_compare_xaxis = list(emotionAfter.keys())
        #     figEmotionComparision = go.Figure(data=[
        #             go.Bar(name='Before', x=emotion_compare_xaxis, y=[emotionBefore[item] for item in emotion_compare_xaxis],marker_color='rgb(76,153,160)'),
        #             go.Bar(name='After', x=emotion_compare_xaxis, y=[emotionAfter[item] for item in emotion_compare_xaxis],marker_color='rgb(62,82,143)')
        #             ])
        #     figEmotionComparision.update_layout(barmode='group',
        #                                           height=400,
        #                                           margin={'l': 30, 'b': 20, 't': 30, 'r': 20},
        #                                           title=dict(text='Emotion Comparision'),
        #                                           yaxis=dict(showgrid=True,showline=True,gridcolor='#eee'),
        #                                           xaxis=dict(showgrid=False,showline=True),
        #                                           plot_bgcolor='rgb(255, 255, 255)')
            
            # divStyle = {'display':'block'}
            # figure_emotion_trend = createSubplotFigure(total_trend_emotion_filterCategory_filterTopics, 5, 2)
            # dictSentimentTrend = {
            #                     'data': [
            #                             go.Bar(
            #                                     x=total_trend_filterCategory_allTopics__time,
            #                                     y=positive_trend_filterCategory_filterTopics,
            #                                     name='Positive',
            #                                     marker=go.bar.Marker(
            #                                             color='#35b14a',
            #                                             )
            #                                     ),
            #                             go.Bar(
            #                                     x=total_trend_filterCategory_allTopics__time,
            #                                     y=neutral_trend_filterCategory_filterTopics,
            #                                     name='Neutral',
            #                                     marker=go.bar.Marker(
            #                                             color='#378fff',
            #                                             )
            #                                     ),
            #                             go.Bar(
            #                                     x=total_trend_filterCategory_allTopics__time,
            #                                     y=negative_trend_filterCategory_filterTopics,
            #                                     name='Negative',
            #                                     marker=go.bar.Marker(
            #                                             color='#d84132',
            #                                             )
            #                                     ),
            #                             go.Scatter(x=[eventValue,eventValue], y=[0,eventLineMaxValue], mode='lines',line=dict(width=2, color='red'))
            #                             ],
            #                             'layout': go.Layout(
            #                                     height=225,
            #                                     title=go.layout.Title(text='Sentiment Trend',xanchor='left',yanchor= 'top',x=0.05,font={'size':10}),
            #                                     showlegend=True,
            #                                     legend=dict(x=0.5, y=1.2,orientation='h'),
            #                                     margin={'l': 30, 'b': 20, 't': 30, 'r': 20},
            #                                     barmode='stack'
            #                                     )
            #                     }
            # dictTotalTrend = {
            #         'data': [
            #                 go.Bar(
            #                         x=total_trend_filterCategory_allTopics__time,
            #                         y=total_trend_filterCategory_filterTopics__value,
            #                         name='Selected Topic',
            #                         marker=go.bar.Marker(
            #                                 color='rgb(60,124,181)',
            #                                 )
            #                         ),
            #                 go.Bar(
            #                         x=total_trend_filterCategory_allTopics__time,
            #                         y=new_totaly,
            #                         name='total',
            #                         marker=go.bar.Marker(
            #                                 color='rgb(205,231,241)',
            #                                 )
            #                         ),
            #                 go.Scatter(x=[eventValue,eventValue], y=[0,eventLineMaxValue], mode='lines',line=dict(width=2, color='red'))
            #                 ],
            #                 'layout': go.Layout(
            #                         showlegend=True,
            #                         barmode='stack'
            #                         )
            #         }
        if True:
            figSentimentComparision = {}
            figEmotionComparision = {}
            divStyle = {'display':'none'}
            figure_emotion_trend = createSubplotFigure(total_trend_emotion_filterCategory_filterTopics, 5, 2)
            dictSentimentTrend = {
                                'data': [
                                        go.Bar(
                                                x=total_trend_filterCategory_allTopics__time,
                                                y=positive_trend_filterCategory_filterTopics,
                                                name='Positive',
                                                marker=go.bar.Marker(
                                                        color='#35b14a',
                                                        )
                                                ),
                                        go.Bar(
                                                x=total_trend_filterCategory_allTopics__time,
                                                y=neutral_trend_filterCategory_filterTopics,
                                                name='Neutral',
                                                marker=go.bar.Marker(
                                                        color='#378fff',
                                                        )
                                                ),
                                        go.Bar(
                                                x=total_trend_filterCategory_allTopics__time,
                                                y=negative_trend_filterCategory_filterTopics,
                                                name='Negative',
                                                marker=go.bar.Marker(
                                                        color='#d84132',
                                                        )
                                                )
                                        ],
                                        'layout': go.Layout(
                                                height=225,
                                                title=go.layout.Title(text='Sentiment Trend',xanchor='left',yanchor= 'top',x=0.05,font={'size':10}),
                                                showlegend=True,
                                                legend=dict(x=0.5, y=1.2,orientation='h'),
                                                margin={'l': 30, 'b': 20, 't': 30, 'r': 20},
                                                barmode='stack'
                                                )
                                }
            dictTotalTrend = {
                    'data': [
                            go.Bar(
                                    x=total_trend_filterCategory_allTopics__time,
                                    y=total_trend_filterCategory_filterTopics__value,
                                    name='Selected Topic',
                                    marker=go.bar.Marker(
                                            color='rgb(60,124,181)',
                                            )
                                    ),
                            go.Bar(
                                    x=total_trend_filterCategory_allTopics__time,
                                    y=new_totaly,
                                    name='total',
                                    marker=go.bar.Marker(
                                            color='rgb(205,231,241)',
                                            )
                                    )
                            ],
                            'layout': go.Layout(
                                    showlegend=True,
                                    barmode='stack'
                                    )
                    }
        dictSentimentNormal = {
                            'data': [
                                    go.Bar(
                                            x=total_trend_filterCategory_allTopics__time,
                                            y=positive_trend_normal_filterCategory_filterTopics,
                                            name='Positive',
                                            marker=go.bar.Marker(
                                                    color='#35b14a',
                                                    )
                                            ),
                                    go.Bar(
                                            x=total_trend_filterCategory_allTopics__time,
                                            y=neutral_trend_normal_filterCategory_filterTopics,
                                            name='Neutral',
                                            marker=go.bar.Marker(
                                                    color='#378fff',
                                                    )
                                            ),
                                    go.Bar(
                                            x=total_trend_filterCategory_allTopics__time,
                                            y=negative_trend_normal_filterCategory_filterTopics,
                                            name='Negative',
                                            marker=go.bar.Marker(
                                                    color='#d84132',
                                                    )
                                            )
                                    ],
                                    'layout': go.Layout(
                                            height=225,
                                            title=go.layout.Title(text='Normalized Sentiment',xanchor='left',yanchor= 'top',x=0.05,font={'size':10}),
                                            showlegend=False,
                                            margin={'l': 30, 'b': 20, 't': 30, 'r': 20},
                                            barmode='stack'
                                            )
                            }
        dictSentiment = {
                'data': [
                        go.Pie(
                                labels=average_sentiment_filterCategory_filterTopics_label,
                                values=average_sentiment_filterCategory_filterTopics_value,
                                hole=0.3,
                                marker=dict(colors=average_sentiment_filterCategory_filterTopics_color),
                                textfont=dict(size=20)
                                ) 
                        ],
                'layout': go.Layout(
                        title=go.layout.Title(text='Sentiment',xanchor='center',yanchor= 'top',x=0.5),
                        margin={'l': 40, 'b': 40, 't': 60, 'r': 40},
                        )
                }

        return dictTotalTrend, dictSentiment,dictSentimentTrend,dictSentimentNormal,figEmotion,figure_emotion_trend,influencerDivChild,format(sum(total_trend_filterCategory_filterTopics__value),',d'),format(tot_inf_twitt_count,',d'), options
    else:
        return {},{},{},{},{},{},[],'','',  options


