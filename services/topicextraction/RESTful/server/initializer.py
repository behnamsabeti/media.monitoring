from flask import Flask
from flask_cors import CORS

# initialization
app = Flask(__name__)
CORS(app, origins="*", allow_headers=[
    "Content-Type", "Authorization", "Access-Control-Allow-Credentials",
    "Access-Control-Allow-Origin", "Access-Control-Allow-Headers"],
     supports_credentials=True, intercept_exceptions=False)