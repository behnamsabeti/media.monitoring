from .debug import print_stack_trace
from .flags import DEBUG


# HELPER PROCEDURE TO UPDATE A TASK'S STATE
def update_task_state(status_table, task_id,
                      task_status, rcode=None,
                      comment=None, progress=None):
    """ Update a task's status """
    try:
        if str(status_table[task_id]).lower() == "killed":
            # if transitioned to 'killed' state
            return None

        status = {"status": task_status}
        if rcode:
            status.update({"rcode": rcode})

        if comment:
            status.update({"comment": comment})

        if isinstance(progress, int) and progress >= 0:
            status.update({"progress": progress})

        status_table[task_id] = status
    except:
        if DEBUG:
            print_stack_trace()
        # if an error occurred while state transition
        return False
    else:
        # if transition was successful(to a non-'killed' state)
        return True
