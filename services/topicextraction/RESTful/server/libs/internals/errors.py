# LIST OF SERVER-SIDE ERRORS THAT MAY OCCUR
SERVICE_UNAVAILABLE = 700
UNKNOWN_ERROR = 701
INVALID_FORMAT = 702
INVALID_CONTENT_TYPE = 730
BAD_BODY_TYPE = 731
BAD_JSON_KEY_VALUE = 732
NON_EXISTING_METHOD = 733
BAD_ARGUMENT_FORMAT = 734
INVALID_ID_TYPE = 735
BAD_ID_LENGTH = 736
MISSING_ID = 737
NONEXISTING_ID = 738
NO_RESULTS = 739
MALFORMED_RESULTS = 740
INVALID_STATE_TRANSITION = 741
EXISTING_USER = 742
INVALID_USERNAME_OR_PASSWORD = 743


errors = {
    SERVICE_UNAVAILABLE: "SERVICE_UNAVAILABLE",
    UNKNOWN_ERROR: "UNKNOWN_ERROR",
    INVALID_FORMAT: "INVALID_FORMAT",
    INVALID_CONTENT_TYPE: "INVALID_CONTENT_TYPE",
    BAD_BODY_TYPE: "BAD_BODY_TYPE",
    BAD_JSON_KEY_VALUE: "BAD_JSON_KEY_VALUE",
    NON_EXISTING_METHOD: "NONEXISTING_METHOD",
    BAD_ARGUMENT_FORMAT: "BAD_ARGUMENT_FORMAT",
    INVALID_ID_TYPE: "INVALID_ID_TYPE",
    BAD_ID_LENGTH: "BAD_ID_LENGTH",
    MISSING_ID: "MISSING_ID",
    NONEXISTING_ID: "NONEXISTING_ID",
    NO_RESULTS: "NO_RESULTS",
    MALFORMED_RESULTS: "MALFORMED_RESULTS",
    INVALID_STATE_TRANSITION: "INVALID_STASTE_TRANSITION",
    EXISTING_USER: 'EXISTING_USER',
    INVALID_USERNAME_OR_PASSWORD: 'INVALID_USERNAME_OR_PASSWORD'
}
