import os
import json
from flask import Response
# TaskServer Internals
from RESTful.methods.internals.methods import METHODS, SERVICE_VERSION, SERVICE_NAME
from RESTful.methods.src.doc_classification import document_classifier
from RESTful.server.libs.internals.debug import print_stack_trace
from RESTful.server.libs.internals.errors import *
from RESTful.server.libs.internals.flags import DEBUG
from flask import abort, request, jsonify
from prometheus_client import Counter
restful_counter = Counter('rest_requests', 'Counter for restful requests')

class TaskServer(object):
    """
    Multi-Threaded Task Server
    """

    def __init__(self, flaskapp):
        self.app = flaskapp

    def run(self, host, port, debug, use_reloader, threaded):
        """
        Starts the task server
        :param host: host ip to bind
        :param port: host port to bind
        :param debug: if 'True' shows the stacktrace on error to browser
        :param use_reloader: if 'True' reloads the new code on its change
        :param threaded: if 'True' server makes a new thread to answer each request
        :return: None
        """

        @self.app.route('/Zaal/api', methods=['POST'])
        def api():
            """
            Accepts a new task
            :return: A HTTP response
            """
            restful_counter.inc()
            if request.headers['Content-Type'] == 'application/json':
                try:
                    d = request.get_json(silent=True)
                    if not isinstance(d, dict):
                        return jsonify({"error": errors[BAD_BODY_TYPE]}), \
                               BAD_BODY_TYPE
                    elif ("method" not in d) or ("args" not in d):
                        return jsonify({"error": errors[BAD_JSON_KEY_VALUE]}), \
                               BAD_JSON_KEY_VALUE
                    elif d["method"] not in METHODS:
                        return jsonify({"error": errors[NON_EXISTING_METHOD]}), \
                               NON_EXISTING_METHOD
                    elif not isinstance(d["args"], dict):
                        return jsonify({"error": errors[BAD_ARGUMENT_FORMAT]}), \
                               BAD_ARGUMENT_FORMAT
                    else:
                        task = {}
                        task.update(d)
                        task.update(self.models.items())

                        if task['method'] == 'document_classifier':
                            results = document_classifier(task)
                            if results['status'] == 'success':
                                return Response(json.dumps({"results": results}, ensure_ascii=False),
                                                content_type='application/json', status=200)
                            else:
                                return Response(json.dumps({"results": results}, ensure_ascii=False),
                                                content_type='application/json', status=results['error_code'])

                except:
                    if DEBUG:
                        print_stack_trace()
                    return jsonify({"error": errors[UNKNOWN_ERROR]}), \
                           UNKNOWN_ERROR
                else:
                    return jsonify({"id": id}), 200
            else:
                return jsonify({"error": errors[INVALID_CONTENT_TYPE]}), \
                       INVALID_CONTENT_TYPE

        @self.app.route('/Zaal/version', methods=['GET'])
        def version():
            """
            Fetches the version number of TaskServer
            :return: A HTTP response
            """
            return jsonify({"version": SERVICE_VERSION, "name": SERVICE_NAME, "message": """Hey you! Welcome to Zaal's API! You can use our API to access Zaal API endpoints. Zaal is a Natural Language Processing (NLP)  service for real world applciations. The first NLP service for Persian Laguage."""}), 200

        @self.app.route('/Zaal/list_methods', methods=['GET'])
        def list_methods():
            """
            Fetches the list of available methods
            :return: A HTTP response
            """
            return jsonify({"version": SERVICE_VERSION, "name": SERVICE_NAME, "methods": METHODS}), 200

        @self.app.route('/Zaal/status/am-i-up', methods=['GET'])
        # @auth.login_required
        def am_i_up():
            """
            Returns ok if the service is up
            :return: A HTTP response
            """
            return jsonify({"status": "Ok"}), 200
        self.app.run(host=host, port=port, debug=debug,
                     use_reloader=use_reloader, threaded=threaded)
