import os

METHODS = ['document_classifier']
SERVICE_VERSION = os.environ.get("VERSION", 'unknown')
SERVICE_NAME = 'Havij'
