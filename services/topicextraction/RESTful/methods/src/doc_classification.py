import numpy as np

from RESTful.methods.src.utils import round_up


def vectorize_doc(document, dic):
    document = document.strip().split()
    document = dic.doc2bow(document)

    return document


def doc_classification(material, document):
    model = material['model']
    dic = material['dictionary']
    t2c = material['topic2class']
    classes = material['classes']

    doc_bow = vectorize_doc(document, dic)
    topic_dist = model[doc_bow]

    dist = np.zeros(len(t2c), dtype=float)
    for item in topic_dist:
        dist[int(item[0])] = float(item[1])

    class_dist = np.matmul(dist, t2c)
    res_dict = {}
    for index in range(len(class_dist)):
        try:
            res_dict[classes[index]] = class_dist[index]
        except Exception as ex:
            print(ex.__str__())
    return res_dict


def type_check(_iterable, _type):
    for i in _iterable:
        result = isinstance(i, _type)
        if not result:
            return False
    return True


def document_classifier(task):
    material = task['topic_model']
    if 'document' not in task['args']:
        return dict(error_code=900, comment="NO DOCUMENT", status='failure')

    if 'document' in task['args'] and not type_check(_iterable=task['args']["document"], _type=str):
        return dict(error_code=901, status="failure", comment="DOCUMENTS MUST BE STRINGS")

    results = doc_classification(material=material,
                                 document=task['args']['document'])

    return dict(status='success', pay_load=dict(zip(map(lambda x: x.replace('/', '_'),
                                                        results.keys()), map(lambda x: round_up(x), results.values()))))
