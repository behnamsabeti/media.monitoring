import numpy as np


def round_up(n, d=4):
    d = int('1' + ('0' * d))
    return float(np.ceil(n * d) / d)


def type_check(_iterable, _type):
    for i in _iterable:
        result = isinstance(i, _type)
        if not result:
            return False
    return True
