import numpy as np
from gensim.corpora import dictionary
from gensim.models import LdaModel


def load_material(model_paths):

    model_file = model_paths['lda_model']
    dic_file = model_paths['dic']
    class_file = model_paths['classes.txt']
    t2c_file = model_paths['topic2class.txt']

    # load model
    model = LdaModel.load(model_file)

    # load dictionary
    dic = dictionary.Dictionary.load(dic_file)

    # load topic2class weights
    topic2class = []

    with open(t2c_file, 'r') as file:
        for line in file:
            line = line.strip().split('\t')
            line = [float(item) for item in line]
            topic2class.append(line)
        topic2class = np.array(topic2class)

    # load classes names
    classes = []

    with open(class_file, 'r') as file:
        for line in file:
            classes.append(line.strip())

    return {'model': model, 'dictionary':  dic, 'topic2class': topic2class, 'classes': classes}