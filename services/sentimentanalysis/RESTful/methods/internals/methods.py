import os

METHODS = ['sentiment_analyzer']
SERVICE_VERSION = os.environ.get("VERSION", 'unknown')
SERVICE_NAME = 'zaal-sentimentanalysis'
