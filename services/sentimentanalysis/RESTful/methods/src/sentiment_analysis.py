import numpy as np
from RESTful.methods.src.utils import round_up
import re


def normalize(comment):
    result = []
    tokens = comment.strip().split()
    for token in tokens:
        token = re.sub(r"(.)\1+", r"\1", token) # remove consequitive chars
        token = re.sub('[^\u0620-\u06D1\u0020]', '', token) # remove non persian words
        token = re.sub(r'[^\w\s]', '', token, re.UNICODE) # remove punctuations
        if token != '':
            result.append(token)
    return result


def sentiment(material: dict, document: str):
    """
    :param material: a dictionary containing the models required for sentiment analysis
    :param document: a unicode string
    :return: a dictionary with signature `Dict[str, float]`
    """
    model = material['model']
    dic = material['dic']
    graph = material['graph']
    token2id = dic.token2id
    text = document
    text = normalize(text)
    data = []
    unknown = len(token2id)
    for word in text:
        index = token2id.get(word)
        if index is not None:
            data.append(index)
        else:
            data.append(unknown)
    if len(data) == 0:
        score = 0
    else:
        data = [data]
        data = np.array(data)
        with graph.as_default():
            score = model.predict(data)
        score = (score * 2) - 1  # scale to [-1,1]
        score = score[0][0]
    result = {'sentiment_score': round_up(score)}
    return result


def type_check(_iterable: 'an iterable object (i.e lists, tuples, ...)',
               _type: 'a type object constructor such as dict, int, ...'):
    for i in _iterable:
        result = isinstance(i, _type)
        if not result:
            return False
    return True


def sentiment_analyzer(task: dict):
    material = task['sentiment_model']
    if 'document' not in task['args']:
        return dict(error_code=900, comment="NO DOCUMENT", status='failure')

    if 'document' in task['args'] and not type_check(_iterable=task['args']["document"], _type=str):
        return dict(error_code=901, status="failure", comment="DOCUMENTS MUST BE STRINGS")

    results = sentiment(material=material,
                        document=task['args']['document'])

    return dict(status='success', pay_load=results)
