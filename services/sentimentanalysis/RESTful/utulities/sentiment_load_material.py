from gensim.corpora import dictionary
from keras.models import load_model
import tensorflow as tf


def load_sentiment(model_paths):
    model_file = model_paths['model']
    dic_file = model_paths['dic']
    dic = dictionary.Dictionary()
    dic = dic.load(dic_file)
    model = load_model(model_file)
    model._make_predict_function()
    graph = tf.get_default_graph()

    return {'model': model, 'dic': dic, 'graph': graph}
