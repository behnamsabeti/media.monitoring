import json
from .debug import print_stack_trace


# use double quotation in dictionaries
class DDict(dict):
    """
    Custom JSON Friendly Dictionary Object
    """

    def __str__(self):
        try:
            return json.dumps(self)
        except:
            print_stack_trace()
            raise Exception("ERROR IN DOUBLE QUOTING THE DICTIONARY")
