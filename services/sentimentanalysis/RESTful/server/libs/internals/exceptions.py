
class ServiceException(Exception):
    
    """ Custom Service Exception Class """
    
    def __init__(self, message, code, agent=None):
        super(Exception, self).__init__()
        self.code = code
        self.message = message
        self.agent = agent
