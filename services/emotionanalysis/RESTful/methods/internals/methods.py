import os

METHODS = ['emotion_analyzer']
SERVICE_VERSION = os.environ.get("VERSION", 'unknown')
SERVICE_NAME = 'zaal-emotionanalysis'
