from RESTful.methods.src.utils import round_up
import re


def normalize(tweet):
    """
    Tweet normalization:
      1. remove newlines
      2. remove non-Persian words
      3. remove punctuations
    """
    clean_tweet = []
    tweet = tweet.replace('\n', ' ')
    words = tweet.strip().split()
    for word in words:
        word = re.sub('[^\u0620-\u06D1\u0020]', '', word)
        if word != '':
            clean_tweet.append(word)
    return clean_tweet


def emotion(material: dict, tweet: str):
    """
    :param material: a dictionary containing the models required for emotion analysis
    :param tweet: a unicode string
    :return: a dictionary with signature `Dict[str, float]`
    """

    model = material['model']
    emoji_scores = material['emoji_scores']
    vectorizer = material['vectorizer']
    graph = material['graph']
    emotions = ['Happy', 'Sad', 'Angry', 'Calm', 'Love', 'Amazed', 'Excited', 'Afraid', 'Frustrated', 'Emotionless']

    clean_tweet = normalize(tweet)

    if len(clean_tweet) == 0:
        result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    else:
        data = vectorizer.transform([' '.join(clean_tweet)])
        with graph.as_default():
            output = model.predict(data)
        output = output[0]
        result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for index in range(len(output)):
            for e in range(len(result)):
                result[e] += output[index] * emoji_scores[index][e]

    emotions_result = {}
    for index in range(len(result)):
        emotions_result[emotions[index]] = round_up(result[index])

    return emotions_result


def type_check(_iterable: 'an iterable object (i.e lists, tuples, ...)',
               _type: 'a type object constructor such as dict, int, ...'):
    for i in _iterable:
        result = isinstance(i, _type)
        if not result:
            return False
    return True


def emotion_analyzer(task: dict):
    material = task['emotion_model']
    if 'tweet' not in task['args']:
        return dict(error_code=900, comment="NO TWEET", status='failure')

    if 'tweet' in task['args'] and not type_check(_iterable=task['args']["tweet"], _type=str):
        return dict(error_code=901, status="failure", comment="TWEETS MUST BE STRINGS")

    results = emotion(material=material, tweet=task['args']['tweet'])

    return dict(status='success', pay_load=results)
