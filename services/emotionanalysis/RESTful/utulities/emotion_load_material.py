import pickle
from keras.models import load_model
import tensorflow as tf


def load_emotion(model_paths):
    model_file = model_paths['model']
    emji_scores_file = model_paths['emoji_scores']
    vectorizer_file = model_paths['vectorizer']
    emoji_scores = []
    with open(emji_scores_file, 'r') as f:
        for line in f:
            s = line.strip().split(',')
            s = [float(token) for token in s]
            emoji_scores.append(s)

    vectorizer = pickle.load(open(vectorizer_file, 'rb'))
    model = load_model(model_file)
    model._make_predict_function()
    graph = tf.get_default_graph()

    return {'model': model, 'vectorizer': vectorizer, 'graph': graph, 'emoji_scores': emoji_scores}
