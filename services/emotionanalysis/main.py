import os
import sys
import grpc
import yaml
import logging
from concurrent import futures
from RESTful.server.initializer import app
import EmotionAnalyzer_pb2
import EmotionAnalyzer_pb2_grpc
from RESTful.server.server import TaskServer, restful_counter
from RESTful.server.libs.internals.debug import print_stack_trace
from RESTful.utulities.emotion_load_material import load_emotion
from GRPC.methods.src.emotion_analysis import emotion_analyzer
from prometheus_client import start_http_server, CollectorRegistry, Counter

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
rpc_counter = Counter('rpc_requests', 'Counter for grpc requests')


def read_config(file_path):
    logger.warning("Trying to load `" + file_path + "`")
    with open(file_path) as stream:
        try:
            return yaml.load(stream)
        except yaml.YAMLError as exc:
            logger.error("Problem in reading the file " + file_path + exc.__str__())


class Service(EmotionAnalyzer_pb2_grpc.EmotionAnalyzerServicer):
    def GetEmotion(self, request, context):
        rpc_counter.inc()
        return EmotionAnalyzer_pb2.EmotionResponse(
            results=emotion_analyzer(tweet=request.document.document,
                material=emotion_model))


if __name__ == "__main__":
    try:
        # port and host can be set via EVs or a config file

        config = read_config(file_path='conf/default-conf.yml')
        restful_port = int(os.environ.get("RESTFUL_SERVICE_PORT", config['restful_service_port']))
        grpc_port = int(os.environ.get("GRPC_SERVICE_PORT", config['grpc_service_port']))
        prometheus_client_port = int(os.environ.get("PROMETHEUS_METRIC_PORT", config['prometheus_client_port']))
        host = os.environ.get("SERVICE_IP", config['service_ip'])

        max_workers = int(os.environ.get("MAX_WORKERS", config['max_workers']))
        emotion_model = load_emotion(model_paths=config['emotion_model'])

        logger.info("Starting Prometheus client at http://"
                     + host + ':' + str(prometheus_client_port))
        registry = CollectorRegistry()
        registry.register(rpc_counter)
        registry.register(restful_counter)
        start_http_server(prometheus_client_port, registry=registry)

        server = grpc.server(futures.ThreadPoolExecutor(max_workers=max_workers))
        EmotionAnalyzer_pb2_grpc.add_EmotionAnalyzerServicer_to_server(Service(), server)
        server.add_insecure_port(host + ':' + str(grpc_port))
        server.start()
        logger.info("Serving a GRPC API at http://" + host + ':' + str(grpc_port))

        # frontend server
        ts = TaskServer(flaskapp=app)
        ts.models = {"emotion_model": emotion_model}
        ts.run(host=host, port=restful_port, debug=True, use_reloader=False, threaded=True)
        logger.info("Serving a RESTful API at http://" + host + ':' + str(restful_port))

    except SystemExit:
        sys.exit(0)
    except:
        print_stack_trace()
        sys.exit(-1)
