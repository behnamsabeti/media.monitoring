# Twitter Emotion Analysis as a single RPC/REST service in Python

Current Supported Emotions:
- Happy
- Sad
- Angry
- Calm
- Love
- Amazed
- Excited
- Afraid
- Frustrated
- Emotionless
