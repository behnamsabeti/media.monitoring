import dash
import dash_core_components as dcc
import dash_html_components as html
import base64
from vision import telegram
from vision import twitter
from vision import news
from vision.Search import search_bar

from vision.app import app

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])

news_fileName = 'vision/images/news-icon.png'
teleg_fileName = 'vision/images/telegram-icon.png'
tweet_fileName = 'vision/images/twitter-icon.png'
mediaMonitoring_fileName = 'vision/images/MediaMonitoring.png'
eventLogo_fileName = 'vision/images/event-logo.png'
query_filename = 'vision/images/query_icon.png'

encoded_image_news = base64.b64encode(open(news_fileName, 'rb').read())
encoded_image_teleg = base64.b64encode(open(teleg_fileName, 'rb').read())
encoded_image_tweet = base64.b64encode(open(tweet_fileName, 'rb').read())
encoded_image_media = base64.b64encode(open(mediaMonitoring_fileName, 'rb').read())
encoded_image_query = base64.b64encode(open(query_filename, 'rb').read())

index_page = html.Div([
    html.Div([
        html.Div([html.H1('Media Monitoring Service', className='LogoTextStyle')], className='Width90Percent FloatLeft')
    ], className='FullWidth FloatLeft'),
    html.Div([
        html.Img(src='data:image/png;base64,{}'.format(encoded_image_media.decode()), className='LandingImageStyle')
    ], className='FullWidth FloatLeft'),
    html.Div([
        html.P('''With this tool we process the ediorial content of media sources on a continuing basis, and then, identifying,
saving and analyzing content that contains specific keywords or topics.
''', className='LandingImageTextStyle')
    ], className='Width70Percent FloatLeft LandingImageTextDivStyle'),
    html.Div([html.H1('AI Engine Query Builder', className='BoxesTitleTextStyle')],
             className='FullWidth FloatLeft BoxesSplitLineStyle'),
    html.Div([
        html.A([
            html.Img(src='data:image/png;base64,{}'.format(encoded_image_query.decode()),
                     className='SourceBoxImageStyle')
        ], href='/query'),
        html.P('This section builds query and uses AI engine to generate analytics.',
               className='SourceBoxTextStyle')
    ], className='SourceBoxDivStyle'),
    #####################################################################################################################################################################################
    html.Div([html.H1('Analytics Results', className='BoxesTitleTextStyle')],
             className='FullWidth FloatLeft BoxesSplitLineStyle'),
    #################################################################################################### stand alone analysis ###########################################################
    html.Div([
        html.A([
            html.Img(src='data:image/png;base64,{}'.format(encoded_image_tweet.decode()),
                     className='SourceBoxImageStyle')
        ], href='/twitter'),
        html.P('Twitter Analytics',
               className='SourceBoxTextStyle')
    ], className='SourceBoxDivStyle'),
    html.Div([
        html.A([
            html.Img(src='data:image/png;base64,{}'.format(encoded_image_news.decode()),
                     className='SourceBoxImageStyle')
        ], href='/news'),
        html.P('News Analytics',
               className='SourceBoxTextStyle')
    ], className='SourceBoxDivStyle MarginLeft5Percent'),
    html.Div([
        html.A([
            html.Img(src='data:image/png;base64,{}'.format(encoded_image_teleg.decode()),
                     className='SourceBoxImageStyle')
        ], href='/telegram'),
        html.P('Telegram Analytics',
               className='SourceBoxTextStyle')
    ], className='SourceBoxDivStyle MarginLeft5Percent'),

], className='LandingBodyDivStyle')


# Update the index
@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/news':
        return news.layout
    elif pathname == '/telegram':
        return telegram.layout
    elif pathname == '/twitter':
        return twitter.layout
    elif pathname == '/query':
        return search_bar.layout
    else:
        return index_page
    # You could also return a 404 "URL not found" page here


if __name__ == '__main__':
    app.run_server(debug=True)
