from elasticsearch import Elasticsearch
import json

pagination = 1000
es = Elasticsearch(['localhost:9200'])

keywords = []
with open('keywords.txt', 'r', encoding='utf8') as keyword_file:
    for line in keyword_file:
        keywords.append(line.strip())

query_body = []
indexes = ['tweets', 'index', 'telegram']
fields = ['body', 'news_content', 'content']

for field in fields:
    # body = {
    #     "query": {
    #         "match": {
    #             field: "|".join(keywords)
    #         },
    #         "range": {
    #             "date": {
    #                 "gte": "now-1y",
    #                 "lte": "now",
    #             }
    #         }
    #     },
    #     "size": pagination
    # }
    # body = {
    #     'query': {
    #         'filtered': {
    #             'filter': {
    #                 'range': {
    #                     "date": {
    #                         "gte": "now-1y",
    #                         "lte": "now",
    #                     }
    #                 }
    #             },
    #             'query': {
    #                 "match": {
    #                     field: "|".join(keywords)
    #                 },
    #             }
    #         }
    #     }
    # }
    body = {"query": {
        "bool": {
            "must": {
                "match": {
                    field: "|".join(keywords)
                },
            },
            "filter": {
                "range": {
                    "date": {
                        "gte": "now-1y",
                        "lte": "now",
                    }
                }
            }
        }
    },
        "size": pagination
    }
    query_body.append(body)

for i, index in enumerate(indexes):
    query = query_body[i]
    print(f'dumping {index}...')
    output = open(f'{index}.json', 'w', encoding='utf8')
    res = es.search(index=index, body=query, scroll='1m', request_timeout=1000)
    total_hits = res['hits']['total']
    print(f'total hits: {total_hits}, dumping {pagination} at a time')
    total = 0
    while True:
        for item in res['hits']['hits']:
            r = json.dumps(item)
            output.write(r + '\n')
            total += 1
        output.flush()
        scroll_id = res['_scroll_id']
        body = {
            "scroll": "1m",
            "scroll_id": scroll_id
        }
        res = es.scroll(body=body)
        if total % pagination == 0:
            print(f'{total} / {total_hits} dumped for {index}')
        if total >= total_hits:
            break
    output.close()
    print(f'{index} dumped')
