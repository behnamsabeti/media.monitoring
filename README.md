# Media Minning Service

## Installation

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python3 main.py 
```

## Architecture
<img src=images/System%20Detailed%20Architecture.jpeg alt="System Architecture" width="600"/>

## NLP Services
1. Topic Modeling
2. Sentiment Analysis
3. Emotion Analysis

## Sources
1. Twitter
2. Telegram
3. News Outlets

## Data
Please contact administrator for data authorization.

## Docker
In order to use Docker for deployment:

```
sudo docker build --no-cache -t media.monitoring:1.0.0 .
sudo docker-compose up -d
```