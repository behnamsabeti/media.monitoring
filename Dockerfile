FROM python:3.7.9-slim

WORKDIR /media.monitoring
COPY . /media.monitoring

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git

RUN pip3 install -r requirements.txt

CMD ["python3", "main.py"]