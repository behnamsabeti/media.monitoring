import json
import os
from typing import List
import config
import numpy as np


class Query:
    def __init__(self):
        self.base_location = '/hdd/data'
        self.result_base = config.result_base_path
        self.telegram_file = os.path.join(self.base_location, 'telegram.json')
        self.twitter_file = os.path.join(self.base_location, 'tweets.json')
        self.news_file = os.path.join(self.base_location, 'index.json')

    def __load_tweets(self, keywords, n_tweets):
        tweets = []
        with open(self.twitter_file, 'r', encoding='utf8') as tweets_file:
            for line in tweets_file:
                data = json.loads(line.strip())
                data = data['_source']
                for kw in keywords:
                    if kw in data['body']:
                        tweets.append(data)
                        break
                if len(tweets) >= n_tweets:
                    break
        return tweets

    def __load_telegram(self, keywords, n_posts):
        telegram_posts = []
        with open(self.telegram_file, 'r', encoding='utf8') as tele_file:
            for line in tele_file:
                data = json.loads(line.strip())
                data = data['_source']
                for kw in keywords:
                    if kw in data['content']:
                        telegram_posts.append(data)
                        break
                if len(telegram_posts) >= n_posts:
                    break
        return telegram_posts

    def __load_news(self, keywords, n_news):
        news = []
        with open(self.news_file, 'r', encoding='utf8') as news_file:
            for line in news_file:
                data = json.loads(line.strip())
                data = data['_source']
                for kw in keywords:
                    if kw in data['news_content']:
                        news.append(data)
                        break
                if len(news) >= n_news:
                    break
        return news

    def __build_news_trend(self, data):
        trend = []
        influencers = {}
        for news in data:
            current_trend = {}
            current_trend['date'] = news['date'].split('T')[0]
            topics = {}
            for t in news['topics']:
                topics[t['name']] = t['score']
            current_trend['topics'] = topics
            current_trend['domain'] = news['domain']
            trend.append(current_trend)
            if news['domain'] in influencers:
                influencers[news['domain']] += 1
            else:
                influencers[news['domain']] = 1
        influencers_list = []
        for domain, count in influencers.items():
            influencers_list.append({
                'domain': domain,
                'count': count
            })
        return sorted(trend, key=lambda k: k['date']), sorted(influencers_list, key=lambda k: k['count'], reverse=True)

    def __build_tweets_trend(self, data):
        trend = []
        influencers = {}
        for tweet in data:
            current_trend = {}
            current_trend['date'] = tweet['date'].split('T')[0]
            default_emotions = {'Sad': 0, 'Love': 0, 'Amazed': 0, 'Excited': 0, 'Frustrated': 0, 'Happy': 0, 'Angry': 0,
                                'Calm': 0, 'Emotionless': 0, 'Afraid': 0}
            for e in tweet['emotions']:
                default_emotions[e['name']] = e['score']
            current_trend['emotion'] = default_emotions
            current_trend['retweet'] = tweet['isRetweet']
            current_trend['profanity'] = tweet['profanity']
            default_sentiment = {'Positive': 0, 'Negative': 0, 'Neutral': 0}
            default_sentiment[tweet['sentiment']['name']] = abs(tweet['sentiment']['score'])
            current_trend['sentiment'] = default_sentiment
            trend.append(current_trend)
            if tweet['author'] in influencers:
                influencers[tweet['author']].append(tweet['url'])
            else:
                influencers[tweet['author']] = [tweet['url']]
        influencers_list = []
        for author, urls in influencers.items():
            influencers_list.append({
                'author': author,
                'tweets': len(urls),
                'urls': urls
            })
        return sorted(trend, key=lambda k: k['date']), sorted(influencers_list, key=lambda k: k['tweets'], reverse=True)

    def __build_telegram_trend(self, data):
        trend = []
        influencers = {}
        for post in data:
            current_trend = {}
            current_trend['date'] = post['date'].split('T')[0]
            topics = {}
            for t in post['topics']:
                topics[t['name']] = t['score']
            current_trend['topics'] = topics
            trend.append(current_trend)
            if post['channelId'] in influencers:
                influencers[post['channelId']].append(int(post['viewed']))
            else:
                influencers[post['channelId']] = [int(post['viewed'])]
        influencers_list = []
        for channel, count in influencers.items():
            posts = len(count)
            view = int(np.sum(count))
            influencers_list.append({
                'channel': channel,
                'view': view,
                'posts': posts
            })
        return sorted(trend, key=lambda k: k['date']), sorted(influencers_list, key=lambda k: k['view'], reverse=True)

    def __save_json(self, data, location):
        with open(location, 'w', encoding='utf8') as writer:
            for d in data:
                writer.write(json.dumps(d) + '\n')
        writer.close()

    def query(self, query_name, keywords: List[str], n_tweets, n_telegram, n_news):
        twitter_data = self.__load_tweets(keywords, n_tweets)
        twitter_trend, twitter_influencers = self.__build_tweets_trend(twitter_data)
        print('twitter trend built.')

        telegram_data = self.__load_telegram(keywords, n_telegram)
        telegram_trend, telegram_influencers = self.__build_telegram_trend(telegram_data)
        print('telegram trend built.')

        news_data = self.__load_news(keywords, n_news)
        news_trend, news_influencers = self.__build_news_trend(news_data)
        print('news trend built.')

        influencers = [
            {'source': 'tweets', 'influencers': twitter_influencers},
            {'source': 'telegram', 'influencers': telegram_influencers},
            {'source': 'news', 'influencers': news_influencers},
        ]
        # if not os.path.exists(os.path.join(self.result_base, query_name)):
        #     os.mkdir(os.path.join(self.result_base, query_name))
        # self.__save_json(influencers, os.path.join(self.result_base, query_name, 'influencers.json'))
        # self.__save_json(twitter_trend, os.path.join(self.result_base, query_name, 'twitter_trend.json'))
        # self.__save_json(telegram_trend, os.path.join(self.result_base, query_name, 'telegram_trend.json'))
        # self.__save_json(news_trend, os.path.join(self.result_base, query_name, 'news_trend.json'))
        print(f'{query_name} query build and saved.')


# if __name__ == '__main__':
#     query = Query()
#     query.query('test', ['روحانی'], 1000, 1000, 1000)
