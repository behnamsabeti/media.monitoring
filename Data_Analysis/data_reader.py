import json
from collections import Counter
import operator
import math
import re


def read_telegram_data():
    output = open('telegram_posts.txt', 'w', encoding='utf8')
    counter = 0
    valid = 0
    with open('Data/taxi-data/taxi-telegram.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            data = str(data['content'])
            tokens = data.strip().split()
            new_tokens = []
            for t in tokens:
                t = re.sub('[^\u0620-\u06D1\u0020]', ' ', t)  # remove non persian words
                t = re.sub(r'[^\w\s]', '', t, re.UNICODE)
                t = t.strip().split()
                for item in t:
                    if item.__len__() != 0:
                        new_tokens.append(item.strip())
            if len(new_tokens) > 10:
                doc = ' '.join(new_tokens)
                valid += 1
                output.write(doc.strip() + '\n')
            counter += 1
            if counter % 10000 == 0:
                print(counter)
    print('number of telegram posts : {} , valid : {}'.format(counter, valid))
    output.close()


def read_news_data():
    output = open('news.txt', 'w', encoding='utf8')
    counter = 0
    valid = 0
    with open('Data/taxi-data/taxi-news.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            data = str(data['news_content'])
            tokens = data.strip().split()
            new_tokens = []
            for t in tokens:
                t = re.sub('[^\u0620-\u06D1\u0020]', ' ', t)  # remove non persian words
                t = re.sub(r'[^\w\s]', '', t, re.UNICODE)
                t = t.strip().split()
                for item in t:
                    if item.__len__() != 0:
                        new_tokens.append(item.strip())
            if len(new_tokens) > 5:
                doc = ' '.join(new_tokens)
                valid += 1
                output.write(doc.strip() + '\n')
            counter += 1
            if counter % 10000 == 0:
                print(counter)
    print('number of news : {} , valid : {}'.format(counter, valid))
    output.close()


def read_twitter_data():
    output = open('tweets.txt', 'w', encoding='utf8')
    counter = 0
    valid = 0
    keyword = 'اسنپ'
    with open('Data/taxi-data/taxi-tweets.json', 'r', encoding='utf8') as file:
        for line in file:
            line = json.loads(line)
            data = str(line['body'])
            retweet = line['isRetweet']
            tokens = data.strip().split()
            new_tokens = []
            for t in tokens:
                t = re.sub('[^\u0620-\u06D1\u0020]', ' ', t)  # remove non persian words
                t = re.sub(r'[^\w\s]', '', t, re.UNICODE)
                t = t.strip().split()
                for item in t:
                    if item.__len__() != 0:
                        new_tokens.append(item.strip())
            if len(new_tokens) > 5 and not retweet and keyword in new_tokens:
                doc = ' '.join(new_tokens)
                valid += 1
                output.write(doc.strip() + '\n')
            counter += 1
            if counter % 10000 == 0:
                print(counter)
    print('number of tweets : {} , valid : {}'.format(counter, valid))
    output.close()


def extract_total_trend():
    output = open('news_trend.csv', 'w')
    counter = 0
    dates = []
    with open('Data/taxi-data/taxi-news.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            date = data['date']
            tokens = date.strip().split('T')
            date = tokens[0]
            dates.append(date)
            counter += 1
            if counter % 10000 == 0:
                print(counter)
    dates = Counter(dates)
    print('total days : {}'.format(len(dates.items())))
    dates = sorted(dates.items(), key=operator.itemgetter(0))
    for date in dates:
        d = date[0]
        t = date[1]
        line = d + ',' + str(t)
        output.write(line + '\n')
    output.close()


def extract_telegram_influencers():
    output = open('telegram_influencers.csv', 'w')
    counter = 0
    channels = {}
    with open('Data/taxi-data/taxi-telegram.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            channel = str(data['channelId'])
            domain = data['domain']
            if channel.isdigit():
                channel = domain
            views = str(data['viewed'])
            if views != '':
                if views.endswith('K'):
                    views = views.split('K')
                    views = views[0]
                    views = float(views)
                    views = int(views*1000)
                elif views.endswith('M'):
                    views = views.split('M')
                    views = views[0]
                    views = float(views)
                    views = int(views * 1000000)
                else:
                    views = int(views)
                if channel not in channels:
                    channels[channel] = (1, views)
                else:
                    temp_posts = channels[channel][0]
                    temp_views = channels[channel][1]
                    channels[channel] = (temp_posts + 1, temp_views + views)
            counter += 1
            if counter % 10000 == 0:
                print(counter)
    print('total channels : {}'.format(len(channels.items())))
    new_channels = {}
    for ch in channels:
        name = ch
        posts = channels[ch][0]
        views = channels[ch][1]
        avg = 0
        if posts != 0:
            avg = views / posts
        new_channels[name] = (posts, views, avg)
    channels = new_channels
    channels = sorted(channels.items(), key=lambda x: x[1][0])
    channels.reverse()
    for ch in channels:
        name = ch[0]
        posts = ch[1][0]
        views = ch[1][1]
        avg = ch[1][2]
        line = name + ',' + str(posts) + ',' + str(views) + ',' + str(avg)
        output.write(line + '\n')
    output.close()


def extract_twitter_influencers():
    output = open('twitter_influencers.csv', 'w', encoding='utf8')
    counter = 0
    users = {}
    with open('Data/taxi-data/taxi-tweets.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            user = str(data['author'])
            retweet = str(data['isRetweet'])
            url = str(data['url'])
            val = 5
            if retweet:
                val = 1
            if user not in users:
                users[user] = (val, url)
            else:
                temp_val = users[user][0]
                users[user] = (temp_val + val, url)
            counter += 1
            if counter % 10000 == 0:
                print(counter)
    print('total users : {}'.format(len(users.items())))
    users = sorted(users.items(), key=lambda x: x[1][0])
    users.reverse()
    for u in users:
        line = u[0] + ',' + str(u[1][0]) + ',' + str(u[1][1])
        output.write(line + '\n')
    output.close()


def extract_news_influencers():
    output = open('news_influencers.csv', 'w')
    counter = 0
    domains = {}
    with open('Data/taxi-data/taxi-news.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            domain = str(data['domain'])
            if domain not in domains:
                domains[domain] = 1
            else:
                domains[domain] += 1
            counter += 1
            if counter % 10000 == 0:
                print(counter)
    print('total domains : {}'.format(len(domains.items())))
    domains = sorted(domains.items(), key=lambda x: x[1])
    domains.reverse()
    for d in domains:
        line = d[0] + ',' + str(d[1])
        output.write(line + '\n')
    output.close()


def extract_twitter_sentiment():
    output = open('twitter_sentiment.csv', 'w', encoding='utf8')
    counter = 0
    sentiment_trend = {}
    with open('Data/taxi-data/taxi-tweets.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            date = data['date']
            tokens = date.strip().split('T')
            date = tokens[0]
            sentiment = data['sentiment']
            tag = sentiment['name']
            score = abs(sentiment['score'])
            pos = 0
            neg = 0
            neu = 0
            if tag == 'Neutral':
                neu = score
            elif tag == 'Positive':
                pos = score
            elif tag == 'Negative':
                neg = score
            else:
                print('error')
                break
            if date not in sentiment_trend:
                sentiment_trend[date] = (pos, neu, neg, 1)
            else:
                temp_pos = sentiment_trend[date][0]
                temp_neu = sentiment_trend[date][1]
                temp_neg = sentiment_trend[date][2]
                temp_count = sentiment_trend[date][3]
                sentiment_trend[date] = (temp_pos + pos, temp_neu + neu, temp_neg + neg, temp_count + 1)
            counter += 1
            if counter % 10000 == 0:
                print(counter)
    print('total dates : {}'.format(len(sentiment_trend.items())))
    new_trend = {}
    for t in sentiment_trend:
        pos = sentiment_trend[t][0]
        neu = sentiment_trend[t][1]
        neg = sentiment_trend[t][2]
        count = sentiment_trend[t][3]
        total = pos + neu + neg
        pos /= total
        neu /= total
        neg /= total
        new_trend[t] = (pos, neu, neg, count)
    sentiment_trend = new_trend
    trend = sorted(sentiment_trend.items(), key=lambda x: x[0])
    for t in trend:
        line = t[0] + ',' + str(t[1][0]) + ',' + str(t[1][1]) + ',' + str(t[1][2]) + ',' + str(t[1][3])
        output.write(line + '\n')
    output.close()


if __name__ == '__main__':
    read_news_data()
