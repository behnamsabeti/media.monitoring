from gensim.corpora import dictionary
from gensim.models import LdaModel, LdaMulticore
import csv
import numpy as np

train_file = 'Model/news/news.txt'
dictionary_file = 'Model/news/dic'
model_file = 'Model/news/lda_model'
topics_file = 'Model/news/topics.csv'


def train_lda():

    topics = 30
    batch_size = 4100
    epochs = 10

    # initialize model
    print('loading dictionary')
    lda_dic = dictionary.Dictionary.load(dictionary_file)

    # model for multi-core processing
    print('initializing the model')
    lda_model = LdaMulticore(num_topics=topics, id2word=lda_dic, workers=30)

    for epoch in range(epochs):

        print('start epoch number {} , total epochs : {}'.format(epoch+1, epochs))

        processed_docs = 0
        batch_counter = 0
        batch = []
        text_file = open(train_file, 'r', encoding='utf8')

        for document in text_file:
            document = document.strip().split()
            document = lda_dic.doc2bow(document)

            if batch_counter < batch_size:
                batch.append(document)
                batch_counter += 1
            else:
                lda_model.update(batch)

                # reset batch for next step
                batch = []
                batch_counter = 0

            processed_docs += 1

            # print progress
            if processed_docs % batch_size == 0:
                print('{} documents processed.'.format(processed_docs))

        # update on last batch
        lda_model.update(batch)

        text_file.close()

    print('model training finished, saving the model...')
    lda_model.save(model_file)


def create_dictionary():

    stopwords = ['و', 'که', 'تو', 'به', 'از', 'یا', 'بی', 'هم', 'رو', 'در', 'این', 'با', 'یه', 'هم', 'اگه', 'تا', 'ها', 'می', 'های', 'ی', 'ای', 'را', 'هر', 'نه', 'من', 'اون', 'چی', 'دیگه', 'داره']

    # batch size for building dictionary
    batch_size = 10000

    text_file = open(train_file, 'r', encoding='utf8')
    lda_dic = dictionary.Dictionary(prune_at=1000000)
    processed_docs = 0
    batch_counter = 0
    corpus = []

    for document in text_file:
        document = document.strip().split()
        if batch_counter < batch_size:
            corpus.append(document)
            batch_counter += 1
        else:
            lda_dic.add_documents(corpus)

            # reset corpus for next batch
            corpus = []
            batch_counter = 0

        processed_docs += 1

        # print progress
        if processed_docs % 10000 == 0:
            print('{} documents processed.'.format(processed_docs))

        # for now just process the first 1 million documents
        # if processed_docs == 1000000:
        #     break

    lda_dic.add_documents(corpus)
    # filter extremes
    lda_dic.filter_extremes(5, 0.9, 400000)

    stops = 0
    for w in stopwords:
        if w in lda_dic.token2id:
            lda_dic.filter_tokens(bad_ids=[lda_dic.token2id[w]])
            stops += 1

    print('stopwords : {}'.format(stops))

    print('dic size : {}'.format(len(lda_dic.items())))

    # save dictionary for next steps
    lda_dic.save(dictionary_file)

    print('total number of processed docs : {}'.format(processed_docs))

    return


def export_topics():

    # num_top_words = 20
    # num_topics = 150

    num_top_words = 30
    num_topics = 30

    # load lda model
    lda_model = LdaModel.load(model_file)

    # open csv file t write topics
    csv_file = open(topics_file, 'w', encoding='utf8')
    writer = csv.writer(csv_file, delimiter=',')

    for id in range(num_topics):
        topic_words = lda_model.show_topic(id, num_top_words)
        topic_words = [topic[0] for topic in topic_words]
        writer.writerow(topic_words)

    csv_file.close()

    return


def load_material(model_file, dic_file, class_file, t2c_file):
    # load model
    model = LdaModel.load(model_file)

    # load dictionary
    dic = dictionary.Dictionary.load(dic_file)

    # load topic2class weights
    topic2class = []

    with open(t2c_file, 'r') as file:
        for line in file:
            line = line.strip().split(',')
            line = [float(item) for item in line]
            topic2class.append(line)
        topic2class = np.array(topic2class)

    # load classes names
    classes = []

    with open(class_file, 'r') as file:
        for line in file:
            classes.append(line.strip())

    return {'model': model, 'dictionary': dic, 'topic2class': topic2class, 'classes': classes}


def extract_topic(material, document, num_of_topics):
    result = {}

    model = material['model']
    dic = material['dictionary']
    t2c = material['topic2class']
    classes = material['classes']

    # vectorize given document
    doc_bow = vectorize_doc(document, dic)
    topic_dist = model[doc_bow]

    dist = np.zeros(num_of_topics, dtype=float)
    for item in topic_dist:
        dist[int(item[0])] = float(item[1])

    class_dist = np.matmul(dist, t2c)

    for index in range(len(class_dist)):
        if class_dist[index] > 0.3:
            result[classes[index]] = class_dist[index]

    if len(result.items()) == 0:
        result['others'] = 1

    return result


def vectorize_doc(document, dic):
    document = document.strip().split()
    document = dic.doc2bow(document)

    return document


if __name__ == '__main__':

    # create and save dictionary
    # create_dictionary()

    # train lda model
    train_lda()

    # # export topics for annotation
    export_topics()
