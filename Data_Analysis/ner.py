import torch
from pytorch_transformers import BertForTokenClassification, BertTokenizer
import hazm
import numpy as np


def ner_service(model: BertForTokenClassification, tokenizer: BertTokenizer, tokens, segments):
    label_list = ['LOC', 'PER', 'DAT', 'ORG', 'LOC', 'O', 'PER', 'EVE', 'DAT', 'ORG', 'EVE']
    label_map = {i: label for i, label in enumerate(label_list)}
    labels = []

    for token in tokens:
        input_ids = torch.tensor([token], dtype=torch.long)
        logits = model(input_ids)
        token_labels = logits[0].detach().cpu().numpy()
        preds = np.argmax(token_labels, axis=2)
        labels.append(preds.tolist())

    result_words = []
    result_labels = []

    for index, sentence in enumerate(tokens):
        words = []
        tags = []

        counter = 0
        word = []
        label = []
        l_temp = labels[index]
        l_temp = l_temp[0]
        for index_token, index_segment in enumerate(segments[index]):
            if index_segment == counter:
                word.append(sentence[index_token])
                label.append(l_temp[index_token])
            elif index_segment > counter:
                counter += 1
                words.append(word)
                tags.append(label)
                word = []
                label = []
                word.append(sentence[index_token])
                label.append(l_temp[index_token])
        result_words.append(words)
        result_labels.append(tags)

    result = []

    for index, words in enumerate(result_words):
        sentence = []

        for w_index, word in enumerate(words):
            curr_word = tokenizer.convert_tokens_to_string(tokenizer.convert_ids_to_tokens(word))
            curr_labels = result_labels[index][w_index]
            curr_label = -1
            for l in curr_labels:
                if l != 5:
                    curr_label = label_map[l]
                    break
            if curr_label == -1:
                curr_label = label_map[5]
            sentence.append((curr_word, curr_label))
        result.append(sentence)
    return result


def tokenize(tokenizer: BertTokenizer, text):
    sentences = hazm.sent_tokenize(text.strip())
    tokens = []
    segments = []
    for sentence in sentences:
        sen_tokens = sentence.split()
        curr_tokens = []
        curr_segments = []
        for i, t in enumerate(sen_tokens):
            temp_tokens = tokenizer.encode(t)
            temp_segments = [i] * len(temp_tokens)
            curr_tokens.extend(temp_tokens)
            curr_segments.extend(temp_segments)
        tokens.append(curr_tokens)
        segments.append(curr_segments)
    return tokens, segments


def normalize(text: str):
    return text.replace('.', ' . ')


def load_model():
    device = torch.device('cpu')
    model = BertForTokenClassification.from_pretrained('D://NER/ner/Models/model/')
    model.to(device)
    tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)
    tokenizer_added = BertTokenizer.from_pretrained('D://NER/ner/Models/vocab/', do_lower_case=False)
    print(len(tokenizer))
    tokenizer.add_tokens(tokenizer_added.added_tokens_decoder)
    print(len(tokenizer))
    print('tokenizer loaded...')
    return model, tokenizer


if __name__ == '__main__':
    model, tokenizer = load_model()
    with open('test.txt', 'r', encoding='utf8') as file:
        text = file.read()
    text = normalize(text)
    tokens, segments = tokenize(tokenizer, text)
    print(len(tokens))
    print(len(segments))
    result = ner_service(model, tokenizer, tokens, segments)
    print('\n')
    print('NER results : \n')
    for sentence in result:
        for word in sentence:
            print(word)
        print('\n')
