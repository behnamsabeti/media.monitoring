import json

keywords = ['روحانی', 'انتخابات', 'رییسی'] # to be replaced with search bar output

telegram_num_docs = 1000
twitter_num_docs = 1000
news_num_docs = 1000

base_location = '/hdd/data/'

telegram_file = open(base_location + 'telegram.json', 'r', encoding='utf8')
twitter_file = open(base_location + 'tweets.json', 'r', encoding='utf8')
news_file = open(base_location + 'index.json', 'r', encoding='utf8')

telegram_data = []
twitter_data = []
news_data = []

# for line in telegram_file:
#     data = json.loads(line.strip())
#     data = data['_source']
#     for kw in keywords:
#         if kw in data['content']:
#             telegram_data.append(data)
#             break
#     if len(telegram_data) >= telegram_num_docs:
#         break
#
for line in twitter_file:
    data = json.loads(line.strip())
    data = data['_source']
    for kw in keywords:
        if kw in data['body']:
            twitter_data.append(data)
            break
    if len(twitter_data) >= twitter_num_docs:
        break

# for line in news_file:
#     data = json.loads(line.strip())
#     data = data['_source']
#     for kw in keywords:
#         if kw in data['news_content']:
#             news_data.append(data)
#             break
#     if len(news_data) >= news_num_docs:
#         break


print(telegram_data[0])
print(telegram_data[-1])

print(twitter_data[0])
print(twitter_data[-1])

print(news_data[0])
print(news_data[-1])
