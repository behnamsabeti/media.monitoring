import operator

from ner import load_model
from ner import normalize
from ner import tokenize
from ner import ner_service
import re

orgs = {}
locs = {}
pers = {}
dates = {}
events = {}


def add_tags(word, tag):
    if tag == 'PER':
        add_to_dic(pers, word)
    elif tag == 'ORG':
        add_to_dic(orgs, word)
    elif tag == 'LOC':
        add_to_dic(locs, word)
    elif tag == 'EVE':
        add_to_dic(events, word)
    elif tag == 'DAT':
        add_to_dic(dates, word)
    else:
        print('error : {}'.format(tag))


def add_to_dic(dic, word):
    if word not in dic:
        dic[word] = 1
    else:
        dic[word] += 1


with open('Association/news.txt', 'r', encoding='utf8') as file:
    model, tokenizer = load_model()
    print('model loaded...')
    c = 0
    for text in file:
        text = normalize(text)
        tokens, segments = tokenize(tokenizer, text)
        result = ner_service(model, tokenizer, tokens, segments)
        for sentence in result:
            prev_word = ''
            for index, word in enumerate(sentence):
                w = word[0]
                tag = word[1]
                if tag != 'O':
                    w = re.sub('[^\u0620-\u06D1\u0020]', ' ', w)
                    w = re.sub(r'[^\w\s]', '', w, re.UNICODE)
                    if w != '':
                        if index == len(sentence)-1 or tag != sentence[index+1][1]:
                            if prev_word != '':
                                w = prev_word + ' ' + w
                            add_tags(w, tag)
                            prev_word = ''
                        else:
                            prev_word = prev_word + ' ' + w
                            prev_word = prev_word.strip()
        c += 1
        print('document {} processed'.format(c))

orgs = sorted(orgs.items(), key=operator.itemgetter(1))
orgs.reverse()
locs = sorted(locs.items(), key=operator.itemgetter(1))
locs.reverse()
pers = sorted(pers.items(), key=operator.itemgetter(1))
pers.reverse()
dates = sorted(dates.items(), key=operator.itemgetter(1))
dates.reverse()
events = sorted(events.items(), key=operator.itemgetter(1))
events.reverse()

dics = [orgs, locs, pers, dates, events]
names = ['orgs.csv', 'locs.csv', 'pers.csv', 'dates.csv', 'events.csv']

for index in range(len(names)):
    with open('Association/' + names[index], 'w', encoding='utf8') as output:
        d = dics[index]
        for item in d:
            line = item[0] + ',' + str(item[1])
            output.write(line + '\n')
        output.close()
