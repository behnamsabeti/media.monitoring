import json
import re
from lda_model import load_material, extract_topic
from ner import load_model, normalize, tokenize, ner_service


def enrich_news_data_with_topics():

    lda_model = 'Model/news/lda_model'
    lda_dic = 'Model/news/dic'
    lda_classes = 'Model/news/topics.txt'
    lda_t2c = 'Model/news/mapping.csv'
    material = load_material(lda_model, lda_dic, lda_classes, lda_t2c)

    output = open('enriched_news.json', 'w', encoding='utf8')
    counter = 0
    with open('Data/taxi-data/taxi-news.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            doc = str(data['news_content'])
            doc = preprocess_text(doc)
            classes = extract_topic(material, doc, 30)
            data['topics'] = classes
            json.dump(data, output)
            output.write('\n')
            counter += 1
            if counter % 1000 == 0:
                print('{} documents processed.'.format(counter))
    print('number of news : {}'.format(counter))
    output.close()


def preprocess_text(data):
    tokens = data.strip().split()
    new_tokens = []
    for t in tokens:
        t = re.sub('[^\u0620-\u06D1\u0020]', ' ', t)  # remove non persian words
        t = re.sub(r'[^\w\s]', '', t, re.UNICODE)
        t = t.strip().split()
        for item in t:
            if item.__len__() != 0:
                new_tokens.append(item.strip())
    if len(new_tokens) != 0:
        return ' '.join(new_tokens)
    else:
        return ' '


def enrich_news_data_with_category():
    output = open('enriched_news.json', 'w', encoding='utf8')
    counter = 0
    valid = 0
    with open('Enriched/news/enriched_news_topics.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            doc = str(data['news_content'])
            title = str(data['title'])
            description = str(data['description'])
            cat = extract_category(doc + ' ' + title + ' ' + description)
            if len(cat) != 0:
                data['ctegory'] = cat
                json.dump(data, output)
                output.write('\n')
                valid += 1
            counter += 1
            if counter % 1000 == 0:
                print('{} documents processed.'.format(counter))
    print('number of news : {} , valid : {}'.format(counter, valid))
    output.close()


def enrich_news_with_ner():
    model, tokenizer = load_model()
    print('model loaded...')

    output = open('enriched_news.json', 'w', encoding='utf8')
    counter = 0
    with open('Enriched/news/enriched_news_topics_category.json', 'r', encoding='utf8') as file:
        for line in file:

            orgs = []
            locs = []
            pers = []
            dates = []
            events = []

            data = json.loads(line)
            doc = str(data['news_content'])
            description = str(data['description'])
            doc = doc + ' ' + description
            text = normalize(doc)
            tokens, segments = tokenize(tokenizer, text)
            result = ner_service(model, tokenizer, tokens, segments)

            for sentence in result:
                prev_word = ''
                for index, word in enumerate(sentence):
                    w = word[0]
                    tag = word[1]
                    if tag != 'O':
                        w = re.sub('[^\u0620-\u06D1\u0020]', ' ', w)
                        w = re.sub(r'[^\w\s]', '', w, re.UNICODE)
                        if w != '':
                            if index == len(sentence) - 1 or tag != sentence[index + 1][1]:
                                if prev_word != '':
                                    w = prev_word + ' ' + w
                                add_tags(w, tag, pers, orgs, locs, events, dates)
                                prev_word = ''
                            else:
                                prev_word = prev_word + ' ' + w
                                prev_word = prev_word.strip()
            ner_tags = {}
            ner_tags['PER'] = pers
            ner_tags['LOC'] = locs
            ner_tags['ORG'] = orgs
            ner_tags['DAT'] = dates
            ner_tags['EVE'] = events
            data['NER'] = ner_tags
            json.dump(data, output)
            output.write('\n')
            counter += 1
            print(counter)
    output.close()


def add_tags(word, tag, pers, orgs, locs, events, dates):
    if tag == 'PER':
        add_to_dic(pers, word)
    elif tag == 'ORG':
        add_to_dic(orgs, word)
    elif tag == 'LOC':
        add_to_dic(locs, word)
    elif tag == 'EVE':
        add_to_dic(events, word)
    elif tag == 'DAT':
        add_to_dic(dates, word)
    else:
        print('error : {}'.format(tag))


def add_to_dic(dic, word):
    if word not in dic:
        dic.append(word)


if __name__ == '__main__':
    enrich_news_with_ner()
