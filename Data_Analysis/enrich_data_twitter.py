import json
import operator
import re
import requests

from lda_model import load_material, extract_topic
# from ner import load_model, normalize, tokenize, ner_service


def enrich_twitter_data_with_topics():

    lda_model = 'Model/twitter/lda_model'
    lda_dic = 'Model/twitter/dic'
    lda_classes = 'Model/twitter/topics.txt'
    lda_t2c = 'Model/twitter/mapping.csv'
    material = load_material(lda_model, lda_dic, lda_classes, lda_t2c)

    output = open('enriched_twitter_topics.json', 'w', encoding='utf8')
    counter = 0
    errors = 0
    with open('Data/taxi-data/taxi-tweet.json', 'r', encoding='utf8') as file:
        for line in file:
            try:
                data = json.loads(line)
                doc = str(data['body'])
                doc = preprocess_text(doc)
                classes = extract_topic(material, doc, 30)
                data['topics'] = classes
                json.dump(data, output)
                output.write('\n')
            except:
                errors += 1
            counter += 1
            if counter % 1000 == 0:
                print('{} documents processed.'.format(counter))
    print('number of news : {} , errors : {}'.format(counter, errors))
    output.close()


def preprocess_text(data):
    tokens = data.strip().split()
    new_tokens = []
    for t in tokens:
        t = re.sub('[^\u0620-\u06D1\u0020]', ' ', t)  # remove non persian words
        t = re.sub(r'[^\w\s]', '', t, re.UNICODE)
        t = t.strip().split()
        for item in t:
            if item.__len__() != 0:
                new_tokens.append(item.strip())
    if len(new_tokens) != 0:
        return ' '.join(new_tokens)
    else:
        return ' '


def enrich_twitter_data_with_category():
    output = open('enriched_twitter_topics_cat.json', 'w', encoding='utf8')
    counter = 0
    valid = 0
    with open('enriched_twitter_topics.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            doc = str(data['body'])
            cat = extract_category(doc)
            if len(cat) != 0:
                data['category'] = cat
                json.dump(data, output)
                output.write('\n')
                valid += 1
            counter += 1
            if counter % 1000 == 0:
                print('{} documents processed.'.format(counter))
    print('number of news : {} , valid : {}'.format(counter, valid))
    output.close()


def enrich_twitter_data_with_sentiment_and_emotion():
    output = open('enriched_twitter_topics_cat_sentiment_emotion.json', 'w', encoding='utf8')
    counter = 0
    valid = 0
    with open('enriched_twitter_topics_cat.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            doc = str(data['body'])
            emotions = emotion_extraction(doc)
            data['emotion'] = emotions
            sentiment = sentiment_extraction(emotions)
            data['sentiment'] = sentiment
            counter += 1
            if counter % 10 == 0:
                print('{} documents processed.'.format(counter))
    print('number of tweets : {} , valid : {}'.format(counter, valid))
    output.close()


def emotion_extraction(doc):
    host = "localhost"
    port = 9094
    task = json.dumps({"method": "emotion_analyzer", "args":
        {"document": doc}}).encode("utf8")

    url = "http://" + host + ":" + str(port) + "/Zaal/api"
    print('request sent')
    r = requests.post(url, auth=('safar-ali', 'xxx'), data=task, headers={'Content-Type': 'application/json'})
    print('recieved')
    res = json.loads(r.text)
    return res['results']['pay_load']


def sentiment_extraction(emotions):
    emotions_names = ['Happy', 'Sad', 'Angry', 'Calm', 'Love', 'Amazed', 'Excited', 'Afraid', 'Frustrated', 'Emotionless']
    tweet_emotions = []
    for e in emotions_names:
        tweet_emotions.append(emotions[e])
    result = dict()
    positive = tweet_emotions[0]
    neutral = tweet_emotions[9]
    negative = tweet_emotions[1] + tweet_emotions[2] + tweet_emotions[7] + tweet_emotions[8]
    rest = tweet_emotions[3] + tweet_emotions[4] + tweet_emotions[5] + tweet_emotions[6]
    if positive > neutral and positive > negative:
        positive += rest
    elif negative > positive and negative > neutral:
        negative += rest
    total = positive + neutral + negative
    positive /= total
    negative /= total
    neutral /= total
    result['positive'] = positive
    result['neutral'] = neutral
    result['negative'] = negative
    return result


def export_enriched_news_data():
    output = open('telegram_trend.json', 'w', encoding='utf8')
    posts = []
    counter = 0
    with open('Enriched/telegram/enriched_telegram_topics_cat_ner.json', 'r', encoding='utf8') as file:
        for line in file:
            data = json.loads(line)
            category = data['category']
            date = data['date']
            tokens = date.strip().split('T')
            date = tokens[0]
            topics = data['topics']
            posts.append((date, category, topics))
            counter += 1
            if counter % 1000 == 0:
                print('{} documents processed.'.format(counter))
    print('{} documents processed.'.format(counter))
    news = sorted(posts, key=operator.itemgetter(0))
    for n in news:
        date = n[0]
        category = n[1]
        topics = n[2]
        posts_dic = {'date': date, 'category': category, 'topics': topics}
        json.dump(posts_dic, output)
        output.write('\n')
    output.close()


if __name__ == '__main__':
    enrich_twitter_data_with_sentiment_and_emotion()
